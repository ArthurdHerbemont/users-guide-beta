.. _chap:proj-organization:

Collaborative Project Development
=================================

.. _multi_developer:

.. rubric:: This chapter

This chapter discusses the options you have in AIMMS to organize a
project in such a way that it becomes possible to effectively work on
the project with multiple developers. While it is very natural to start
working on a project with a single developer, at some time during the
development of an AIMMS application, the operational requirements of the
problem you are modeling may become so demanding that it requires
multiple developers to accomplish the task.

.. rubric:: From prototyping phase...

During the initial prototyping phase of a model, an AIMMS project is
usually still quite small, allowing a single developer to take care of
the complete development of the prototype. The productivity tools of
AIMMS allow you to quickly implement different formulations and analyze
their results, while you are avoiding the overhead of having to
synchronize the efforts of multiple people working on the prototype.

.. rubric:: ...to operational phase

During the development of an operational AIMMS application this
situation may change drastically. When an AIMMS application is intended
to be used on a daily basis, it usually involves one or more of the
following tasks:

-  retrieving the input data from one or more data sources,

-  validation and transformation of input data,

-  extending the core optimization application with various,
   computationally possibly demanding, operational requirements,

-  preparing and writing output data to one or more data sources,

-  building a professionally looking end-user GUI, and/or

-  integrating the application into the existing business environment.

Depending on the size of the application, implementing all of these
tasks may become too demanding for a single developer.

.. rubric:: Dividing a project into sub-projects

One possible approach to allow multiple developers to work on a single
AIMMS application is to divide the project into several logical
sub-projects, either based on the tasks listed in the previous
paragraph, or more closely related to the logic of your application.
AIMMS supports sub-projects in the form of model libraries. Using
libraries especially makes sense, if the functionality developed in a
library can be re-used by multiple AIMMS applications. If a library is
small enough, indivual developers may take on the development of the
library.

.. rubric:: Managing project source using a VCS

In the software development world teams commonly use a *V*\ ersion
*C*\ ontrol *S*\ ystem, such as
`git <http://en.wikipedia.org/wiki/Git_(software)>`__,
`subversion <http://en.wikipedia.org/wiki/Apache_Subversion>`__, or
`TFS <http://en.wikipedia.org/wiki/Team_Foundation_Server>`__, to share
and merge their coding work to a common repository. As all development
sources of an AIMMS application are stored as readable text files
(a.o. ``.aimms``, ``.ams`` and ``.xml``), AIMMS projects can be easily
managed using the version control system of your choice. Using a version
control system will make it straightforward to work together on a single
code base in parallel by merging the contributions of the various team
members, and to use branches to differentiate between development and
production code, or to work on multiple developments independently.
Using version control for your AIMMS projects will usually result in
higher productivity and more control.

.. rubric:: No VCS integration

Although AIMMS effectively supports the use of a version control system
for the development of your AIMMS applications, the AIMMS IDE does not
offer integration with any specific version control system. All version
control systems come with commandline and/or graphical tools for regular
version control tasks such as committing, showing logs, diffing two
versions, merging, creating branches and tags, and so on. You should use
these tools, whenever you want to commit your changes to an AIMMS
project under version control.

.. _sec:proj-organization.manager:

Library Projects and the Library Manager
----------------------------------------

.. rubric:: AIMMS library projects

AIMMS *library projects* allow you to divide a large AIMMS project into
a number of smaller sub-projects. Library projects are especially useful
if you intend to share parts of an application between multiple
projects. Each library project in AIMMS provides

-  a tree of model declarations,

-  a page tree,

-  a template tree, and

-  a menu tree.

In addition, a library project may provide its own collection of user
project files, user colors and fonts.

.. rubric:: Shared templates

Besides enabling multiple developers to work in a single project,
library projects can also be used to define a common collection of
templates that define the look-and-feel of multiple projects. In this
manner, you change the look-and-feel of multiple applications just by
changing the templates in the shared library project.

.. rubric:: Adding libraries to a project

By adding a library project to the main AIMMS project, the objects
defined by the library, such as identifiers, pages, templates, etc.,
become available for use in the main project. In addition, if a library
project is writable, the contents of the library can also be edited
through an AIMMS project in which it is included.

.. rubric:: The library manager

You can add libraries to an AIMMS project in the **AIMMS Library
Manager** dialog box illustrated in :numref:`fig:library-manager`. You
can open the library manager through the **File-Library Manager...** menu.

.. figure:: library-manager.png
   :alt: The **AIMMS Library Manager**
   :name: fig:library-manager

   The **AIMMS Library Manager**

Using the library manager AIMMS allows you to

-  create new libraries,

-  add existing libraries to your project,

-  edit library properties, and

-  remove libraries from your project.

.. rubric:: Library storage

Each library project in AIMMS will be stored in a separate directory,
containing the following files and folders:

-  the ``Project.xml`` file holding a reference to the project's main
   model source file (with an ``.ams`` extension), as well as all
   additional model source files included in the main model source file,
   together containing all identifier declarations, procedures and
   functions that are relevant to the project,

-  ``PageManager.xml``, ``TemplateManager.xml`` and ``MenuBuilder.xml``
   files describing the page, template and menu tree defined in the
   project, with all individual pages and templates being stored in the
   ``Pages`` and ``Templates`` subfolders,

-  ``Settings`` and ``Tools`` subfolders containing the saved settings
   for user colors, fonts, and the various tools in the AIMMS IDE, and

-  the ``User Files`` folder for storing all user files that you store
   within the project.

These files will be automatically created by AIMMS when you create a new
library project. To add an existing library to an AIMMS project, you
just need to select its library project file.

.. rubric:: Library prefix

To avoid name clashes between objects in the library and the main
project or other libraries, all the object names in a library are stored
in a separate namespace. Outside of the library, a global prefix
associated with the library has to be used to access the library
objects. When you create a new library project, AIMMS will come up with
a default library prefix based on the library name you entered. For an
existing library project, you can view and edit its associated library
prefix in the library manager.

.. rubric:: Using library projects

After you have added one or more library projects to your main AIMMS
project, AIMMS will extend

-  the model tree in the **Model Explorer**,

-  the page tree in the **Page Manager**,

-  the template tree in the **Template Manager**, and

-  the menu tree in the **Menu Builder**

with additional root nodes for every library project added to your
project. In general, within any of these tools, you are free to move
information from the main project tree to any of the library trees and
vice versa. In addition, the AIMMS dialog boxes for user project files,
user colors and fonts allow you to select and manage objects from the
main project or any of the libraries. The precise details for working
with library projects in each of these tools are discussed in full
detail in the respective chapters discussing each of the tools.

.. _sec:proj-organization.working:

Guidelines for Working with Library Projects
--------------------------------------------

.. rubric:: Identifying independent tasks

Unless you started using library projects from scratch, you need to
convert the contents of your AIMMS project as soon as you decide to
divide the project into multiple library projects. The first step in
this process is to decide which logical tasks in your application you
can discern that meet the following criteria:

-  the task represents a logical unit within your application that is
   rather self-contained and can possible be shared with other AIMMS
   projects,

-  the task can be comfortably and independently worked on by a separate
   developer or developer team, and

-  the task provides a limited interface to the main application and/or
   the other tasks you have identified.

Good examples of generic tasks that meet these criteria are the tasks
listed on :ref:`chap:proj-organization`. Once your team of developers
agrees on the specific tasks that are relevant for your application, you
can set up a library project for each of them.

.. rubric:: Library interface...

The idea behind library projects is to be able to minimize the
interaction between the library, the main project and other library
projects. At the language level AIMMS supports this by letting you
define an *interface* to the library, i.e. the set of public identifiers
and procedures through which the outside world is allowed to connect to
the library. Library identifiers not in the interface are strictly
private and cannot be referenced outside of the library. The precise
semantics of the interface of a library module is discussed in
:ref:`lr:sec:module.library` of the Language Reference.

.. rubric:: ... used in model and GUI

This notion of public and private identifiers of a library module does
not only apply to the model source itself, but also propagates to the
end-user interface. Pages defined in a library can access the library's
private identifiers, while paged defined outside of the library only
have access to identifiers in the interface of the library.

.. rubric:: Minimal dependency

The concept of an interface allows you to work independently on a
library. As long as you do not change the declaration of the identifiers
and procedures in its interface, you have complete freedom to change
their implementation without disturbing any other project that uses
identifiers from the library interface. Similarly, as long as a page or
a tree of pages defined in a library project is internally consistent,
any other project can add a reference to such pages in its own page
tree. Pages outside of the library can only refer to identifiers in the
library interface, and hence are not influenced by changes you make to
the library's internal implementation.

.. rubric:: Conversion to library projects

If your application already contains model source and pages associated
with the tasks you have identified in the previous step, the next step
is to move the relevant parts of your AIMMS project to the appropriate
libraries. You can accomplish this by simply dragging the relevant nodes
or subtrees from any of the trees tree in the main project to associate
tree in a library project. What should remain in the global project are
the those parts of the application that define the overall behavior of
your application and that glue together the functionality provided by
the separate library projects.
