.. _chap:cases:

Case Management
===============

.. _case_management:

.. rubric:: This chapter

Working with data is a central part of any modeling application. Data
can come from external sources or from AIMMS' proprietary case files.
This chapter introduces AIMMS' capabilities with respect to creating and
managing a collection of case files. Furthermore, AIMMS' capabilities of
working with data from multiple case files, both from within the
language and from within graphical data objects on end-user pages, are
illustrated.

.. rubric:: Not in this chapter

AIMMS uses a proprietary binary format for case files to store data
compactly, quickly, and easily. This propietary format makes case files
unsuitable to exchange data with other programs. AIMMS' capabilities to
exchange data with other programs is documented in the Language
Reference:

* :doc:`lr:data-communication-components/communicating-with-databases/index`
* :doc:`lr:data-communication-components/reading-and-writing-spreadsheet-data/index`
* :doc:`lr:data-communication-components/reading-and-writing-xml-data/index`
* :doc:`lr:advanced-language-components/the-aimms-programming-interface/index`

Furthermore, the AIMMS SDK offers access to AIMMS data from Java,
C#, and C++, see http://download.aimms.com/aimms/AimmsSDK.

Working with Cases
------------------

.. rubric:: Case management tasks

A case file is a single file containing the data of some identifiers in
an AIMMS model. The **Data** menu is the main tool through which you can
accomplish tasks such as saving, loading, merging, and comparing case
files. This menu item is part of the developer menu and is available by
default on all end-user pages.

.. rubric:: The active case

In AIMMS, all the data that you are currently working with, is referred
to as the *active* case. If you have not yet loaded or saved a case
file, the active case is *unnamed*, otherwise the active case is *named*
after the name of the last loaded or saved case file on disk. If the
active case is named, its name is displayed in the status bar at the
bottom of the AIMMS window.

.. rubric:: Saving a case file

When you save a named active case, AIMMS will save it to the associated
case file on disk by default, thereby overwriting its previous contents.
If the active case is unnamed, or when you try to save a case using the
**Data-Save Case As** menu, AIMMS will open the **Save Case** dialog box
illustrated in :ref:`fig:cases.case-save`.

.. _case-save:
.. figure:: case-save.png
   :alt: The **Save Case File** dialog box
   :name: fig:cases.case-save

   The **Save Case File** dialog box

In the **Save Case File** dialog box you can enter the name of the case
file, and, optionally, select the folder in which the case file is to be
stored. After successfully saving a case file through the **Save Case
File** dialog box, the active case will become named.

.. rubric:: Loading a case file

AIMMS supports three modes for loading the data of a case file, as
summarized in the following table:

.. container:: center

   ================= ================== ============ ==========
   \                 **changes name**   **replaces** **merges**
   **mode**          **of active case** **data**     **data**
   load as active           ✓                 ✓        
   load into active                           ✓       
   merge into active                                     ✓
   ================= ================== ============ ==========

The modes are explained in more detail below.

.. rubric:: Load as active

The most frequently used mode for loading a case file is loading the
case file *as active*, through the **Data-Load Case-As Active** menu.
Loading a case file as active completely replaces the active data of all
identifiers in the case file being loaded. Data of identifiers that are
not stored in the case file, remain unchanged. In addition, the active
case will be named after the loaded case file. Before loading a case
file as active, AIMMS will ask you whether the current active case data
needs to be saved whenever this is necessary.

.. rubric:: Load into active

Loading a case file *into active*, through the **Data-Load Case-Into
Active** menu, is completely identical to loading a case as active, with
the exception that the name of the active case will not be changed.
Thus, by loading data into the active case you can replace part, or all,
of the contents of the active case with data obtained from another case
file.

.. rubric:: Merge into active

*Merging* a case file *into active*, through the **Data-Load Case-Merge
Into Active** menu, does not change the name of the active case either.
Merging a case file into the active case partially replaces the data in
the active case with only the nondefault values stored in the loaded
case file. Data in the active case, for which no associated nondefault
values exist in the merged case file, remain unchanged.

.. rubric:: Starting a new case

Using the **Data-New Case** menu item, you can instruct AIMMS to start a
new, unnamed, active case. However, the data in the active case will
remain *unchanged*. Before starting a new case, AIMMS will ask you
whether the current active case data needs to be saved.

.. _sec:cases.casesel:

Managing Multiple Case Selections
---------------------------------

.. rubric:: Viewing multiple case files

AIMMS allows you to simultaneously view the results of several case
files within the graphical user interface. In addition, it is possible
to reference data from multiple case files from within the modeling
language, enabling you to perform advanced forms of case comparison.

.. rubric:: Multiple case selections

AIMMS offers a tool to construct a selection of cases to which you want
simultaneous access, either from within the graphical user interface or
from within the model itself. You can add one or more selected cases
from within the **Data** menu to the multiple case file selection
through the **Data-Multiple Cases** menu. This will open the **Select
Multiple Case Files** dialog box illustrated in :ref:`MultipleCaseSelection`.

.. _MultipleCaseSelection:

.. figure:: MultipleCaseSelection.png
   :alt: The **Select Multiple Case Files** dialog box
   :name: fig:casesel.casesel

   The **Select Multiple Case Files** dialog box

It shows the current contents of the multiple case file selection. You
can modify the order of the displayed cases, and add cases to or delete
cases from the collection.

.. _sec:cases.casesel.view:

Viewing Multiple Case Data
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: Viewing multiple case data

The prime use of multiple case selection takes advantage of AIMMS'
capability of displaying data from multiple cases within its graphical
objects. :ref:`MultipleCaseObject` is an illustration of a
table which displays the contents of a single identifier for all the
cases in the case selection shown in :ref:`MultipleCaseSelection`.

.. _MultipleCaseObject:
.. figure:: MultipleCaseObject.png
   :alt: Example of a multiple case object
   :name: fig:casesel.multiple-case

   Example of a multiple case object

.. rubric:: Creating multiple case objects

A data object on a page in the graphical end-user interface can be
turned into a multiple case object by checking the multiple case
property in the object-specific options in the object **Properties**
dialog box. :ref:`mc-prop-new`. illustrates the
object-specific **Properties** dialog box of a table object.

.. _mc-prop-new:
.. figure:: mc-prop-new.png
   :alt: Table-specific **Properties** dialog box
   :name: fig:casesel.mc-prop

   Table-specific **Properties** dialog box

As a result of enabling multiple case display, the object will be
extended with one additional virtual dimension, the case index, which
will be displayed in a standard way.

.. rubric:: Restrictions

AIMMS only supports the display of multiple case data in object types
for which the added dimension can be made visible in a well-defined
manner. The most important object types that support multiple case
displays are tables, pivot tables, curves, bar charts and scalar
objects. Because of the extra dimension, the bar chart object is only
able to display multiple case data for scalar and 1-dimensional
identifiers. During a single case display, a bar chart can also be used
to view 2-dimensional identifiers.

.. _sec:cases.casesel.language:

Case Referencing from Within the Language
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: Using inactive case data
   :name: multi-case-ref

In addition to viewing data from multiple case files as graphical
objects in the graphical user interface, AIMMS also allows you to
reference the data of case files that are not currently active within
the model. This allows you, for instance, to perform advanced forms of
case file differencing by comparing the current values of particular
identifiers in your model with the corresponding values stored in an
inactive case.

.. rubric:: The set :any:`AllCases`

The collection of all case files referenced via the AIMMS data menu, or
via the intrinsic functions such as :any:`CaseFileLoad`, and
:any:`CaseFileMerge` is available in the AIMMS language through the
predefined integer subset . Each case file is represented by an integer
element in this set, and, as explained in :ref:`sec:gui.functions.case`,
AIMMS offers several built-in functions to obtain additional information
about a case through its case number.

.. rubric:: The set`CurrentCaseSelection`

AIMMS stores the case selection constructed in the **Select Multiple
Case Files** dialog box presented above in the predefined set , which is
a subset of the ever growing set . Through this set you get easy access
within your model to the cases selected by your end-users in the
**Select Multiple Case Files** window.

.. rubric:: Referencing case data

You can reference the values of specific identifiers within a particular
case by simply prefixing the identifier name with an index or element
parameter in the set Allcase or any of its subsets. Thus, if ``cs`` is an index
in the set`CurrentCaseSelection`, the following simple assignment
will inspect every case in the user-selected multiple case selection,
and store the values of the variable ``Transport(i,j)`` stored in that
case in the parameter ``CaseTransport``, which has one additional
dimension over the set of`CurrentCaseSelection`.

.. code-block:: aimms

	CaseTransport(cs,i,j) := cs.Transport(i,j);

.. rubric:: Advanced case comparison

The capability of referencing inactive case data, enables you to perform
advanced forms of case comparison, which would be hard to accomplish
without the AIMMS facilities for case referencing. As an example,
consider the following statement.

.. code-block:: aimms

	RelativeDiff(cs,i,j) := (cs.Transport(i,j) - Transport(i,j)) /$ Transport(i,j);

It computes the relative difference between the current values of the
variable ``Transport(i,j)`` and those values stored for each case
referenced.

.. rubric:: Inactive data

Please note that ``cs.Transport(i,j)`` above, may contain inactive data,
when index ``cs`` refers to the active case. In order to remedy this,
you may want to use the ``CleanUp`` statement, see
:ref:`lr:sec:data.control`, at the start of procedures containing case
referencing.

.. _sec:cases.contentType:

Working with Selections of Identifiers
--------------------------------------

.. rubric:: Case content type

Next to saving the contents *all* identifiers in a case file, it is also
possible to save the data of *a selection of* identifiers in a case
file. Such a selection of identifiers to be saved to a case file is
called a *case content type*. A case content type is a subset of .

.. rubric:: Collection

The set contains all case content types. It is a subset of the
predeclared set . The set :any:`AllCaseFileContentTypes` is initialized to
contain only the case content type . By adding additional subsets of
:any:`AllIdentifiers`, you are allowing your end user to decide which
selection of identifiers is to be saved. The predeclared element
parameter is used to indicate the case content type selected by the
end-user of your application.

.. rubric:: Example

You may add the predeclared set to the set , which allows an end-user to
decide whether to save the data of all identifiers in your model, or
just of the collection of current input parameters. This is illustrated
in :ref:`SaveCaseCT` where the **Save Case File** dialog
box allows you to select between the case content types
:any:`AllIdentifiers` and :any:`CurrentInputs`.

.. _SaveCaseCT:

.. figure:: SaveCaseCT.png
   :alt: The **Save Case File** dialog box offering content types
   :name: fig:cases.case-save-ct

   The **Save Case File** dialog box offering content types

.. rubric:: Use during case load

When loading a case, all identifiers stored in the case file will be
loaded; the current contents of the case content type by which the file
is saved will be ignored.

.. rubric:: Data not stored

Identifiers in an AIMMS model can have the ``NoSave`` property.
Identifiers with this property will not be saved in any case file
regardless of the current case content type. This property can be set
via the attribute forms of the identifiers that can contain data.
