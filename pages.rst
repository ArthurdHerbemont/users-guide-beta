.. _chap:page:

Pages and Page Objects
======================

.. _pages_and_page_objects:

.. rubric:: Creating an end-user interface

After you have created a model in AIMMS to represent and solve a
particular problem, you may want to move on to the next step: creating a
graphical end-user interface around the model. In this way, you and your
end-users are freed from having to enter (or alter) the model data in
text or database tables. Instead, they can make the necessary
modifications in a graphical environment that best suits the purposes of
your model. Similarly, using the advanced graphical objects available in
AIMMS (such as the Gantt chart and network flow object), you can present
your model results in an intuitive manner, which will help your
end-users interpret a solution quickly and easily.

.. rubric:: This chapter

This chapter gives you an overview of the possibilities that AIMMS
offers you for creating a complete model-based end-user application. It
describes pages, which are the basic medium in AIMMS for displaying
model input and output in a graphical manner. In addition, the chapter
illustrates how page objects (which provide a graphical display of one
or more identifiers in your model) can be created and linked together.

.. _sec:page.intro:

Introduction
------------

.. rubric:: What is a page?

A *page* is a window in which the data of an AIMMS model is presented in
a graphical manner. Pages are the main component of an end-user
interface for a model-based decision support application. An example of
an end-user page is given in :numref:`fig:resize-edit-new`.

The page shown here provides a comprehensive graphical overview of the
results of an optimization model by means of a *network flow object* in
which flows which require attention are colored red. By clicking on a
particular flow in the network object, additional information about that
flow is shown in the tables on the left of the page.

.. rubric:: Page design

Pages are fully designed by application developers for use by the
end-users of an application. Thus, you, as a developer, can decide at
what position in the interface particular model data should be presented
to the end-user, and in which format. In addition, by automatically
executing procedures when opening or closing a page or when modifying
data, you can make sure that all the necessary computations are
performed before certain model results are displayed.

.. figure:: resize-edit-new.png
   :name: fig:resize-edit-new

   Example of a end-user page


.. _sec:page.create:

Creating Pages
--------------

.. rubric:: Creating pages

Creating an end-user page is as easy as adding a new node to the page
tree in the **Page Manager** (see :ref:`chap:pagetool`).
:numref:`fig:page.pagetree` illustrates the page tree associated with
the example application used throughout this guide.

.. figure:: page-man-new.png
   :alt: Example of a page tree
   :name: fig:page.pagetree

   Example of a page tree

As all the trees in the AIMMS modeling tools work alike, you can use any
of the methods described in :ref:`sec:model.trees` to add a new page
node to the page tree.

.. rubric:: Copying pages

In addition to inserting a new empty page into the page tree, you can
also copy an existing page or an entire subtree of pages, by either a
simple cut, copy and paste or a drag-and-drop action in the tree (see
:ref:`sec:model.trees`). All copied pages will have the same content as
their originals.

.. rubric:: Page name

The node name of every page (as displayed in the page tree) is the
unique name or description by which the page is identified in the
system. When you add new pages to the tree, AIMMS will name these *Page
1*, *Page 2*, etc. You can change this name using the standard methods
for changing names of tree nodes as described in :ref:`sec:model.trees`.

.. rubric:: Page title

By default, the node name is the title that will be displayed in the
frame of the page window when the page is opened. In the page
**Properties** dialog box (see :ref:`sec:prop.property`) you can,
however, specify a different page title to be displayed, which can
either be a constant string or a reference to a string parameter in the
model. The latter is useful, for instance, if you intend to set up an
end-user interface in multiple languages.

.. rubric:: Position in page tree

Its position in the page tree determines the navigational properties of
the page. It will determine how any button with references to the next
or previous page, or any navigation object or menu linked to the page,
will behave. These navigational aspects of the **Page Manager** tool are
discussed in more detail in :ref:`chap:pagetool`.

.. rubric:: Using templates

Every page that you add to the page tree, is also automatically added to
the template tree in the **Template Manager**. By moving the page to a
different position in the template tree, the page automatically inherits
all the properties such as page size or background, and all objects
specified on the template pages hierarchically above it. The **Template
Manager** and the use of templates is explained in full detail in
:ref:`chap:pagetool`.

.. _sec:page.object:

Adding Page Objects
-------------------

.. rubric:: Page objects

All visible components on a page are instances of the collection of
*page objects* as offered by AIMMS. Such page objects are mostly used to
visualize the input and output data of your model in various ways. They
also include simple drawing objects, such as lines and circles, and
buttons for navigation and execution.

.. rubric:: Edit mode

Before you can add page objects to a page, the page must be in *edit
mode*. When you open a page using the **Page Manager**, it is opened in
*user mode* by default. When you want to open a page in edit mode from
the **Page Manager**, you can do so using the right mouse pop-up menu.
If a page is already opened in user mode, you can reopen it in edit mode
using the |page-edt| button on the page toolbar. When you open the page
from the **Template Manager**, it is opened in edit mode by default.

.. rubric:: Common data objects

AIMMS provides the most common graphical data objects such as

-  row-oriented composite tables,

-  2-dimensional tables,

-  pivot tables,

-  graphs, and

-  charts.

These objects can be used both for displaying and for modifying the data
in your model. The data displayed in such objects are always directly
linked to one or more identifiers in your model.

.. rubric:: Adding an object

Placing a data object onto a page can be done without any programming.
The following straightforward actions are required:

-  select the type of the graphical object to be displayed,

-  drag a rectangle onto the page of the intended size of the object,
   and

-  choose the identifier in the model holding the data that you want to
   display.

.. rubric:: Selecting the object type

You can select the object type that you want to add to the page from the
**Object** menu. Alternatively, you can select any of the most common
object types using the **Page Edit** toolbar, as depicted in
:numref:`fig:page.toolbar`.

.. figure:: page-tb-new.png
   :alt: The **Page Edit** toolbar
   :name: fig:page.toolbar

   The **Page Edit** toolbar

If you move the cursor to one of the buttons of the toolbar, a tooltip
will appear. After you have selected an object type, the page cursor
will change to a cross allowing you to drag the rectangle in which the
object will be contained. :numref:`fig:page.draw` illustrates such a
rectangle just prior to linking it to one or more AIMMS identifiers.

.. figure:: obj-reg-new.png
   :alt: An identified substructure causing infeasibility
   :name: fig:page.draw

   An identified substructure causing infeasibility

.. rubric:: Object grid and alignment

In order to let you drag object regions in an aligned manner, AIMMS
allows you to associate a grid with a page, and align object regions to
that grid automatically via the **View** menu. Alternatively, you may
align objects later on, or make them the same size via the
**Edit-Alignment** menu (see :ref:`sec:prop.select`).

.. rubric:: Selecting an identifier ...

After you have indicated the object region, you must select an
identifier to be associated with that object. To support you in this
task AIMMS provides an **Identifier Selection** dialog box as
illustrated in :numref:`fig:page.id-select`. You can select any single
identifier from the list on the right.

.. rubric:: ...from a subselection

Additional help is offered for models with many identifiers. By
selecting a subtree of the model tree on the left-hand side of the
dialog box, you can narrow down the selection of identifiers on the
right-hand side to those which are declared within the selected subtree.
With the **Filter...** button you can narrow the selection down even
more, by only displaying those identifier types that you are interested
in.

.. rubric:: Selecting from a library

When your project contains one or more library projects, AIMMS only
allows you to select identifiers that are part of the interface of a
library on any page not included in such a library (see also
:ref:`sec:proj-organization.working`). If the page is part of the page
tree of a library, AIMMS allows you to select from *all* the identifiers
declared in the library.

.. rubric:: Ensuring your freedom

By restricting access from within pages outside of the library to the
identifiers in the library interface only, AIMMS allows you to freely
modify the internal implementation of your library. No other part of the
application will be inflicted if you make changes to identifier
declarations that are not included in the library interface.

.. figure:: id-sel.png
   :alt: The **Identifier Selection** dialog box
   :name: fig:page.id-select

   The **Identifier Selection** dialog box

.. rubric:: Slices and linking

In its simplest form, you can use the **Identifier Selection** dialog
box to select an entire identifier of the appropriate dimension to fill
a selected object. However, the **Identifier Selection** dialog box will
also let you consider selecting *slices* of identifiers, or provide
automatic *links* between objects. These advanced subjects will be
discussed in detail in :ref:`sec:page.slice` below.

.. rubric:: Object properties

After you have selected the identifier(s) necessary to fill the page
object with the appropriate model data, AIMMS will draw the object using
default settings for properties such as fonts, colors and borders. Later
on, you can change these properties (or even modify the defaults) via
the **Properties** dialog box of the object (see also
:ref:`sec:prop.property`).

.. rubric:: Example

If the object region displayed in :numref:`fig:page.draw` is used to
draw a table object, and the identifier selection dialog box in
:numref:`fig:page.id-select` is used to select the identifier
``NodeCoordinate(n,crd)``, the table in :numref:`fig:page.table`
results.


.. figure:: new-tbl-new.png
   :alt: Example of a newly created table object
   :name: fig:page.table

   Example of a newly created table object

.. _sec:page.object.expression:

Displaying Expressions in Page Objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: Displaying expressions

In addition to indexed identifiers, AIMMS also allows you to display
expressions in a page object. This is convenient, for instance, when you
want to display some data which is not directly available in your model
in the form of an (indexed) identifier, but which can be easily computed
through an expression referring to one or more identifiers in your
model. In such a case, you do not have to create an additional defined
parameter containing the expression that you want to display, but rather
you can directly enter the expression in the **Identifier Selection**
dialog box, as illustrated in :numref:`fig:page.expression-select`.

.. figure:: id-sel-expr-new.png
   :alt: Selecting an expression in a page object
   :name: fig:page.expression-select

   Selecting an expression in a page object

.. rubric:: Entering an expression

When you have indicated that the page object should display an
expression rather than an indexed identifier, AIMMS will display the
**Expression Definition** dialog box illustrated in
:numref:`fig:page.expression-enter`. In this dialog box you must specify
the exact definition of the expression you want to be displayed in the
page object.

.. figure:: id-sel-expr2-new.png
   :alt: Entering an expression for a page object
   :name: fig:page.expression-enter

   Entering an expression for a page object

.. rubric:: Specifying the expression type

In the **Expression Type** field of the **Expression Definition** dialog
box, you must select the type of the expression you entered. AIMMS only
allows the display of

-  numeric,

-  element-valued, and

-  string-valued.

expressions. AIMMS does not allow the display of set expressions. If the
expression type is element-valued, you must also indicate the element
range of the expression, i.e. the set in which the expression will hold
its values.

.. rubric:: Specifying the index domain

Finally, in the **Index Domain** field of the **Expression Definition**
dialog box you must specify the index domain over which the expression
is defined. Contrary to the ``IndexDomain`` attribute in a parameter
declaration form, AIMMS only accepts a list of indices in this field,
i.e. you cannot add a domain condition (see also :ref:`lr:sec:par.decl`
of the Language Reference). If you want to restrict the domain of the
expression, you specify the domain condition as a ``$`` condition within
the expression definition (see also :ref:`lr:sec:expr.cond` of the
Language Reference). This is illustrated in
:numref:`fig:page.expression-enter`, where ``MeasuredFlow(f)`` serves as
a domain condition on the domain ``f``.

.. _sec:page.object.advanced:

Creating Advanced Page Objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. rubric:: Advanced data objects ...

In addition to common graphical data objects such as tables, bar charts
and curves, AIMMS also supports a number of advanced graphical objects.
These objects are designed for specialized, but widely-used, application
areas. The most notable advanced objects available in AIMMS are:

-  an interactive *Gantt chart* for time-phased scheduling and planning
   applications, and

-  a *network flow object* for applications in which two-dimensional
   maps or flows play a central role.

.. rubric:: ...are based on multiple identifiers

Advanced data objects have the characteristic that multiple model
identifiers are required to represent the visual result. For instance,
in the network flow object you need a set identifier to denote the set
of nodes to be displayed and their coordinates in the network, as well
as a parameter to indicate the flow values between these nodes.
:numref:`fig:page.network` illustrates the selection dialog box of a
network flow object.

.. figure:: new-nwob-new.png
   :alt: Identifier selection for the network flow object
   :name: fig:page.network

   Identifier selection for the network flow object

To enter the appropriate identifiers for each required component, you
can open the common **Identifier Selection** dialog box described above
by pressing the wizard button |wizard| at the right of each
individual component.

.. rubric:: Object help

In this `User's Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__ you will only find the basic mechanisms for adding
or modifying pages and page objects. Full details of all object types,
and their properties and settings, are described in the on-line help
facility which is always available when you are running AIMMS.

.. rubric:: Non-data objects

In addition to data-related objects, AIMMS also supports various other
types of objects such as:

-  drawing objects (such as line, circle, rectangle, picture and text
   objects), and

-  buttons to initiate model execution and page navigation.

Drawing objects and buttons are positioned on a page in exactly the same
manner as the data objects described above, except that a link to one or
more AIMMS identifiers is not required.

.. _sec:page.slice:

Selecting Identifier Slices and Linking Objects
-----------------------------------------------

.. _slicing_identifiers:

.. rubric:: Advanced identifier selection

After you have selected an indexed identifier (or expression) in the
**Identifier Selection** dialog box, a second dialog box appears, as
illustrated in :numref:`fig:page.id-select2`.

.. figure:: id-sel2-new.png
   :alt: Advanced **Identifier Selection** options
   :name: fig:page.id-select2

   Advanced **Identifier Selection** options

In this dialog box, you have several options to refine your choice, each
of which will be described in this section.

.. rubric:: Slicing and subset restriction

By default, AIMMS assumes that you want to associate the full identifier
with the object in hand. However, with the dialog box of
:numref:`fig:page.id-select2` AIMMS allows you to modify several
domain-related issues before displaying the identifier. More
specifically, for every individual dimension in the index domain of the
identifier, you can:

-  restrict that dimension to those elements that are included in a
   particular subset associated with the domain set by using a subset
   index,

-  select a slice of the identifier by fixing that dimension to the
   value of a particular scalar element-valued parameter that assumes
   its values into the corresponding domain set, or

-  select a slice of the identifier by fixing that dimension to a
   specific element in the corresponding domain set.

In the dialog box of :numref:`fig:page.id-select2` AIMMS lets you select
specific elements, element parameters or subset indices on the
right-hand side of the dialog box to restrict the dimension that is
selected on the left-hand side.

.. rubric:: Dimension reduction

By fixing a particular dimension to an element parameter or a set
element, the total number of dimensions of the displayed data is reduced
by one. Thus, by fixing one dimension of a two-dimensional parameter,
only a one-dimensional table will be displayed. The number of dimensions
is not reduced when the display is restricted to elements in a subset.
In this case, however, the object will display less data.

.. rubric:: Index order and table split

For a table object, the **Identifier Selection** dialog box also lets
you determine the order of the dimensions and a split of the dimensions.
This allows you to specify which dimensions are shown rowwise and which
columnwise, and in which order. If you do not insert a split manually,
AIMMS will determine a default split strategy.

.. rubric:: Index linking

Finally, the identifier selection options offer you the possibility of
establishing a link between a particular dimension of the selected
identifier and a (scalar) element parameter that assumes its values into
the corresponding domain set. As an example, consider the dialog box of
:numref:`fig:page.id-select2`. In it, the dimension ``nfrom`` of the
identifier ``FlowMap(f,nfrom,nto)`` is linked to the element parameter
``SourceNode``, and the dimension ``nto`` to the element parameter
``DestinationNode``.

.. rubric:: Link notation

In the **Properties** dialog boxes of a linked object, AIMMS displays
the link using a "``->``" arrow. Thus, the parameter ``FlowMap`` from
the example above, will be displayed as

.. code-block:: aimms

	FlowMap( f, nfrom -> SourceNode, nto -> DestinationNode )

This special link notation is only valid in the graphical interface, and
cannot be used anywhere else in the formulation of your model.

.. rubric:: Effect of index linking

When the identifier ``FlowMap(f,nfrom,nto)`` is displayed in, for
instance, a table object, AIMMS will, as a result of the specified index
links, automatically assign the values of ``nfrom`` and ``nto``
associated with the currently selected table entry to the element
parameters ``SourceNode`` and ``DestinationNode``, respectively.

.. rubric:: Use of index linking

Index linking is a very powerful AIMMS feature that allows you to
effectively implement several attractive features in an end-user
interface without any programming effort on your part. Some
representative uses of index linking are discussed below.

-  You can use index links involving one or more element parameters in a
   particular page object as a way of triggering AIMMS to automatically
   update one or more other page objects that contain identifier slices
   fixed to these element parameters. These updates will occur as soon
   as a user clicks somewhere in the particular page object in which the
   indices were linked. An illustrative example of such automatic
   linkage of page objects is shown below.

-  You can use index linking to keep track of the current user selection
   in an object when executing a procedure within your model. This
   allows you to do some additional data processing, or perform some
   necessary error checks for just that tuple in a multidimensional
   identifier, whose value has most recently been modified by the
   end-user of your application.

.. rubric:: Example

Consider the page shown in :numref:`fig:page.link`.

.. figure:: inherit-new.png
   :alt: Entering an expression for a page object
   :name: fig:page.link

   Example of index linking

The tables and lists underneath the **flow data** text in the center
part of the page display detailed information regarding the currently
selected flow in the network flow object shown in the right part of the
page. This was accomplished as follows. The index ``f`` representing the
flows in the network flow object on the right was linked to a single
element parameter ``FlowEl`` in the set ``Flows``. The tables and lists
on the left of the screen contain identifier slices fixed to the element
parameter ``FlowEl``. Take, for instance, the values in the column named
``Measured`` in the table object on the lower left part of the screen.
This column corresponds to the one-dimensional identifier slice
``MappedMeasuredComposition(c,FlowEl)``. As a result of the link, the
column ``Measured`` automatically displays detailed information for the
flow selected by the end- user in the flow chart on the right.

.. |page-edt| image:: page-edt.png

.. |wizard| image:: wizard.png
