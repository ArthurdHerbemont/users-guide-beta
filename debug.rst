.. _chap:debug:

Debugging and Profiling an AIMMS Model
======================================

.. rubric:: This chapter

After you have developed an (optimization) model in AIMMS, it will most
probably contain some unnoticed logical and/or programming errors. These
errors can cause infeasible solutions or results that are not entirely
what you expected. Also, you may find that the execution times of some
procedures in your model are unacceptably high for their intended
purpose, quite often as the result of only a few inefficiently
formulated statements. To help you isolate and resolve such problems,
AIMMS offers a number of diagnostic tools, such as a debugger and a
profiler, which will be discussed in this chapter.

.. _sec:debug.debug:

The AIMMS Debugger
------------------

.. _debugger:

.. rubric:: Tracking modeling errors

When your model contains logical errors or programming errors, finding
the exact location of the offending identifier declarations and/or
statements may not be easy. In general, incorrect results might be
caused by:

-  incorrectly specified attributes for one or more identifiers declared
   in your model (most notably in the ``IndexDomain`` and ``Definition``
   attributes),

-  logical oversights or programming errors in the formulation of one or
   more (assignment) statements in the procedures of your model,

-  logical oversights or programming errors in the declaration of the
   variables and constraints comprising a mathematical program, and

-  data errors in the parametric data used in the formulation of a
   mathematical program.

.. rubric:: Errors in mathematical programs

If the error is in the formulation or input data of a mathematical
program, the main route for tracking down such problems is the use of
the **Math Program Inspector** discussed in :ref:`chap:mpinspector`.
Using the **Math Program Inspector** you can inspect the properties of
custom selections of individual constraints and/or variables of a
mathematical program.

.. rubric:: The AIMMS debugger

To help you track down errors that are the result of misformulations in
assignment statements or in the definitions of defined parameters in
your model, AIMMS provides a *source debugger*. You can activate the
AIMMS debugger through the **Tools-Diagnostic Tools-Debugger** menu.
This will add a **Debugger** menu to the system menu bar, and, in
addition, add the **Debugger** toolbar illustrated in
:numref:`fig:debug.toolbar` to the toolbar area.

.. figure:: toolbar-dbg-new.png
   :alt: The **Debugger** toolbar
   :name: fig:debug.toolbar

   The **Debugger** toolbar

You can stop the AIMMS debugger through the **Debugger-Exit Debugger**
menu.

.. rubric:: Debugger functionality

Using the AIMMS debugger, you can

-  set conditional and unconditional breakpoints on a statement within
   the body of any procedure or function of your model, as well as on
   the evaluation of set and parameter definitions,

-  step through the execution of procedures, functions and definitions,
   and

-  observe the effect of single statements and definitions on the data
   within your model, either through tooltips within the observed
   definitions and procedure bodies, or through separate data pages (see
   also :ref:`sec:decl.data`)

.. rubric:: Setting breakpoints in the body

Within the AIMMS debugger you can set breakpoints on any statement in a
procedure or function body (or on the definition of a defined set,
parameter or variable) by selecting the corresponding source line in the
body of the procedure or function, and choosing the
**Debugger-Breakpoints-Insert/Remove** menu (or the **Insert/Remove
Breakpoint** button |breakpoint-button-set| on the **Debugger** toolbar). After you
have set a breakpoint, this is made visible by means of red dot in the
left margin of selected source line, as illustrated in
:numref:`fig:debug.breakpoint`.


.. figure:: breakpoint-proc-new.png
   :alt: Setting a breakpoint in a procedure body
   :name: fig:debug.breakpoint

   Setting a breakpoint in a procedure body

.. rubric:: Setting breakpoints in the model tree

Alternatively, you can set a breakpoint on a procedure, function or on a
defined set, parameter or variable by selecting the corresponding node
in the **Model Explorer**, and choosing the
**Debugger-Breakpoints-Insert/Remove** menu. As a result, AIMMS will add
a breakpoint to the first statement contained in the body of the
selected procedure or function. The name of a node of any procedure,
function or defined set, parameter or variable with a breakpoint is
displayed in red in the model tree, as illustrated in
:numref:`fig:debug.explorer`.

.. figure:: breakpoint-explorer-new.png
   :alt: Viewing procedures with breakpoints in the **Model Explorer**
   :name: fig:debug.explorer

   Viewing procedures with breakpoints in the **Model Explorer**

.. rubric:: Entering the debugger

Once you have set a breakpoint in your model, AIMMS will automatically
stop at this breakpoint whenever a line of execution arrives at the
corresponding statement. This can be the result of

-  explicitly running a procedure within the **Model Explorer**,

-  pushing a button on an end-user page which results in the execution
   of one or more procedures, or

-  opening an end-user (or data) page, which requires the evaluation of
   a defined set or parameter.

Whenever the execution stops at a breakpoint, AIMMS will open the
corresponding procedure body (or the declaration form of the defined
set, parameter or variable), and show the current line of execution
through the breakpoint pointer |breakpoint-pointer|, as illustrated in
:numref:`fig:debug.break`.

.. figure:: breakpoint-at-active-new.png
   :alt: Arriving at a breakpoint
   :name: fig:debug.break

   Arriving at a breakpoint

.. rubric:: Interrupting execution

Even when you have not set breakpoints, you can still enter the debugger
by explicitly *interrupting* the current line of execution through the
**Run-Stop** menu (or through the **Ctrl-Shift-S** shortcut key). It
will pop up the **Stop Run** dialog box illustrated in
:numref:`fig:debug.interrupt`

.. figure:: interrupt-new.png
   :alt: The **Stop Run** dialog box
   :name: fig:debug.interrupt

   The **Stop Run** dialog box

When you have activated the AIMMS debugger prior to execution, the
**Debug** button on it will be enabled, and AIMMS will enter the
debugger when you push it. By pushing the **OK** or **Cancel** button,
AIMMS will completely stop or just continue executing, respectively.

.. rubric:: Interrupting slow statements

The above method of interrupting AIMMS will not work when AIMMS is
executing a statement or definition that takes a very long time. In that
case you can interrupt AIMMS via the ``AimmsInterrupt`` tool. This tool
is available from the Windows start All Programs menu. Upon startup, it
will place itself in the system tray. By right-clicking the AIMMS system
tray icon, you'll obtain a menu of running AIMMS instances that can be
interrupted. In developer mode, the interrupted AIMMS will also popup a
debugger showing where it has been interrupted. With that debugger, you
can't continue execution, however; as the consistency of the values of
the identifier(s) being computed during the interrupt can't be
guaranteed. On the other hand, you can start new procedures. In end-user
mode, the interrupted AIMMS will just issue an error message, indicating
the interrupted statement, definition or constraint.

.. rubric:: Stepping through statements

Once AIMMS has interrupted a line of execution and entered the debugger,
you can step through individual statements by using the various step
buttons on the **Debugger** toolbar and follow the further flow of
execution, or observe the effect on the data of your model. AIMMS offers
several methods to step through your code:

-  the **Step Over** |breakpoint-button-step-over| method runs a single statement, and,
   when this statement is a procedure call, executes this in its
   entirety,

-  the **Step Into** |breakpoint-button-step-into| method runs a single statement, but,
   when this statement is a procedure call, sets the breakpoint pointer
   to the first statement in this procedure,

-  the **Step Out** |breakpoint-button-step-out| method runs to the end of the current
   procedure and sets the breakpoint pointer to the statement directly
   following the procedure call in the calling context, and

-  the **Run To Cursor** |breakpoint-button-run-to-cursor| method runs in a single step from
   the current position of the breakpoint pointer to the current
   location of the cursor, which should be *within the current
   procedure*.

In addition, AIMMS offers some methods to continue or halt the
execution:

-  the **Continue Execution** |breakpoint-button-continue| method continues execution,
   but will stop at any breakpoint it will encounter during this
   execution,

-  the **Finish Execution** |breakpoint-button-finish| method finishes the current line
   of execution, ignoring any breakpoints encountered,

-  the **Halt** |breakpoint-button-halt| method immediately halts the current line of
   execution.

.. rubric:: Examining identifier data

Whenever you are in the debugger, AIMMS allows you to interactively
examine the data associated with the identifiers in your model, and
observe the effect of statements in your source code. The most
straightforward method is by simply moving the mouse pointer over a
reference to an identifier (or identifier *slice*) within the source
code of your model. As a result, AIMMS will provide an overview of the
data contained in that identifier (slice) in the form of a tooltip, as
illustrated in :numref:`fig:debug.tooltip`.

The tooltip will provide global information about the identifier slice
at hand, such as

-  its name and indices,

-  the number of elements or non-default data values (in brackets), and

-  the first few elements or non-default data value in the form of a
   list consisting of tuples and their corresponding values.

.. figure:: breakpoint-proc-tooltip-new.png
   :alt: Observing the current data of an identifier through a tooltip
   :name: fig:debug.tooltip

   Observing the current data of an identifier through a tooltip

.. rubric:: Detailed identifier data

If you need to examine the effect of a statement on the data of a
particular identifier in more detail, you can simply open a **Data
Page**, as described in :ref:`sec:decl.data`, or observe the effect on
ordinary end-user pages. Within a debugger session, AIMMS supports data
pages for both global and local identifiers, thereby allowing you to
examine the contents of local identifiers as well. After each step in
the debugger AIMMS will automatically update the data on any open
end-user or data page.

.. rubric:: Breakpoint on data change

If you are not sure which statement in your model is responsible for
changing the data of a (non-defined) set or parameter, you can set a
breakpoint on such a set or parameter. Whenever a statement in your
model changes the set or parameter data at hand, AIMMS will break on
that statement. Notice, however, that breakpoint on data change will not
pick up data changes that are due to set or parameter data becoming
inactive because of changes to sets or parameters included in the domain
or domain condition.

.. rubric:: Viewing the call stack

Whenever you are in the debugger, the **Call Stack** button |breakpoint-button-call-stack|
on the **Debugger** toolbar will display the **Call Stack** dialog box
illustrated in :numref:`fig:debug.call-stack`.

.. figure:: breakpoint-call-stack-new.png
   :alt: The **Call Stack** dialog box
   :name: fig:debug.call-stack

   The **Call Stack** dialog box

With it you get a detailed overview of the stack of procedure calls
associated with the current line of execution. It enables you to observe
the flow of execution at the level of procedures associated with the
current position of the breakpoint pointer. After selecting a procedure
or definition in the **Call Stack** dialog box, the **Show Position**
button will open its attribute window at the indicated line.

.. rubric:: Viewing and modifying breakpoints

After you have inserted a number of breakpoints into your model, you can
get an overview of all breakpoints through the **Show All Breakpoints**
button |breakpoint-button-list|. This button will invoke the **List of Breakpoints**
dialog box illustrated in :numref:`fig:debug.bp-list`.

.. figure:: breakpoint-list-new.png
   :alt: The **List of Breakpoints** dialog box
   :name: fig:debug.bp-list

   The **List of Breakpoints** dialog box

For each breakpoint, AIMMS will indicate whether it is enabled or
disabled (i.e. to be ignored by the AIMMS debugger). Through the buttons
on the right hand side of the dialog box you can

-  disable breakpoints,

-  enable previously disabled breakpoints,

-  delete breakpoints, and

-  create new breakpoints.

Alternatively, you can disable or remove all breakpoints simultaneously
using the **Disable All Breakpoints** button |breakpoint-button-disable| and the
**Remove All Breakpoints** button |breakpoint-button-remove|.

.. rubric:: Conditional breakpoints

In addition, by pushing the **Condition** button on the **List of
Breakpoints** dialog box, you can add a condition to an existing
breakpoint. It will open the **Breakpoint Condition** dialog box
illustrated in :numref:`fig:debug.condition`.

.. figure:: breakpoint-condition-new.png
   :alt: The **Breakpoint Condition** dialog box
   :name: fig:debug.condition

   The **Breakpoint Condition** dialog box

The condition must consist of a simple numerical, element or string
comparison. This simple comparison can only involve scalar identifiers,
identifier slices or constants. Free indices in an identifier slice are
only allowed when they are fixed within the breakpoint context
(e.g. through a for loop). AIMMS will only stop at a conditional
breakpoint, when the condition that you have specified is met during a
particular call. Conditional breakpoints are very convenient when, for
instance, a procedure is called very frequently, but only appears to
contain an error in one particular situation which can be detected
through a simple comparison.

.. _sec:debug.profile:

The AIMMS Profiler
------------------

.. _profiler:

.. rubric:: Meeting time requirements with solvers

Once your model is functionally complete, you may find that the overall
computational time requirement set for the application is not met. If
your application contains optimization, and most of the time is spent by
the solver, finding a remedy for the observed long solution times may
not be easy. In general, it involves finding a reformulation of the
mathematical program which is more suitable to the selected solver.
Finding such a reformulation may require a considerable amount of
expertise in the area of optimization.

.. rubric:: Meeting time requirements with data execution

It could also be, however, that optimization (if any) only consumes a
small part of the total execution time. In that case, the time required
for executing the application is caused by data manipulation statements.
If total execution time is unacceptably high, it could be caused by
inefficiently formulated statements. Such statements force AIMMS to fall
back to *dense* instead of *sparse* execution. :ref:`lr:chap:sparse` and
:ref:`lr:chap:eff` of the Language Reference discuss the principles of
the sparse execution engine used by AIMMS, and describe several common
pitfalls together with reformulations to remedy them.

.. rubric:: The AIMMS profiler

AIMMS offers a profiler to help you resolve computational time related
issues. The AIMMS profiler enables you to locate the most time-consuming
evaluations of

-  procedures and functions,

-  individual statements within procedures and functions,

-  defined sets and parameters, and

-  constraints and defined variables during matrix generation.

.. rubric:: Activating the profiler

You can activate the AIMMS profiler by selecting the **Tools-Diagnostic
Tools-Profiler** menu, which will add a **Profiler** menu to the default
system menu bar. If the debugger is still active at this time, it will
be automatically deactivated, as both tools cannot be used
simultaneously.

.. rubric:: Gathering timing information

As soon as you have activated the profiler, AIMMS will start gathering
timing information during every subsequent procedure run or definition
evaluation, regardless whether these are initiated by pushing a button
on an end-user page, by executing a procedure from within the **Model
Explorer**, or even by means of a call to the AIMMS API from within an
external DLL.

.. rubric:: Viewing profiler results

After you have gathered timing information about your modeling
application by executing the relevant parts of your application at least
once, you can get an overview of the timing results through the
**Profiler-Results Overview** menu. This will open the **Profiler
Results Overview** dialog box illustrated in
:numref:`fig:debug.prof-res`.

.. figure:: profiler-results-new.png
   :alt: The **Profiler Results Overview** dialog box
   :name: fig:debug.prof-res

   The **Profiler Results Overview** dialog box

In it, you will find a list of all procedures that have been executed
and identifier definitions that have been evaluated since the profiler
was activated.

.. rubric:: Detailed timing information

For each procedure, function, or defined identifier listed in the
**Profiler Results Overview** dialog box, AIMMS will provide the
following information:

-  the number of hits (i.e. the number of times a procedure has been
   executed or a definition has been evaluated),

-  the total gross time (explained below) spent during all hits,

-  the total net time (explained below) spent during all hits,

-  the average gross time spent during each separate hit, and

-  the average net time spent during each separate hit.

.. rubric:: Gross versus net time

The term *gross time* refers to the total time spent in a procedure
*including* the time spent in procedure calls or definition evaluations
within the profiled procedure. The term *net time* refers to the total
time spent *excluding* the time spent in procedure calls or definition
evaluations within the profiled procedure.

.. rubric:: Locating time-consuming procedures

With this timing information you can try to locate the procedures and
identifier definitions which are most likely to benefit from a
reformulation to improve efficiency. To help you locate these procedures
and definitions, the list of procedures and definitions in the
**Profiler Results Overview** dialog box can be sorted with respect to
all its columns. The most likely criterion for this is to sort by
decreasing net time or average net time, which will identify those
procedures and identifier definitions which take up the most time by
themselves, either in total or for each individual call. You can open
the attribute form of any identifier in the **Profiler Results
Overview** dialog box by simply double-clicking on the corresponding
line.

.. rubric:: Locating offending statements

When you have located a time-consuming procedure, you can can open its
attribute form and try to locate the offending statement(s). Whenever
the profiler has been activated, AIMMS will add additional profiling
columns to the body of a procedure, as illustrated in
:numref:`fig:debug.prof-attr`.

Similarly, AIMMS will add these profiling columns to the definition
attributes of defined identifiers.

.. figure:: profiler-attributes-new.png
   :alt:  Profiling information in an attribute form
   :name: fig:debug.prof-attr

   Profiling information in an attribute form

.. rubric:: Profiling column information

For each statement in the body of a procedure, AIMMS can display various
types of profiling data in the profiling columns of an attribute form.
As you can see next, this information is even more extensive than for
procedures as a whole. The following information is available:

-  the number of hits (i.e. the number of times a particular statement
   has been executed),

-  the total gross time spent during all hits,

-  the total net time spent during all hits,

-  the average gross time spent during each separate hit, and

-  the average net time spent during each separate hit.

.. rubric:: Gross versus net time for particular statements

In the context of a procedure body, the difference between gross and net
time need not always refer only to the time spent in other procedures
(as in the **Profiler Results Overview** dialog box). For selected
statements both numbers may have a somewhat different, yet meaningful,
interpretation. Some exceptions:

-  in flow control statements such as the ``IF``, ``WHILE`` and ``FOR``
   statement (see also :ref:`lr:sec:exec.flow` of the Language
   Reference), the net time refers to the time required to evaluate the
   statement itself (for instance, its condition) whereas the gross time
   refers to the time required to execute the entire statement,

-  in the ``SOLVE`` statement (see also :ref:`lr:sec:mp.solve` of the
   Language Reference), the net time refers to the time spent in the
   solver, while the gross time refers to the time spent in the solver
   plus the time required to generate the model.

-  in a procedure call itself the net time refers to the time spent in
   argument passing.

.. rubric:: Profiler tooltips

In addition to observing the profiling times in the **Profiler Results
Overview** and through the profiling columns in attribute windows, AIMMS
also provides profiling tooltips in both the **Model Explorer** and in
the attribute windows of procedures and defined identifiers, as long as
the profiler is active. An example of a profiling tooltip is given in
:numref:`fig:debug.prof-tooltip`.

.. figure:: profiler-tooltip-tree-new.png
   :alt: Observing profiling information through a tooltip
   :name: fig:debug.prof-tooltip

   Observing profiling information through a tooltip

Profiling tooltips can provide a convenient method to quickly observe
the profiling information without requiring any further interaction with
AIMMS. If you do not want AIMMS to display profiling tooltips while
moving your mouse through either the **Model Explorer** or procedure
bodies, you can disable them through the **Profiler Setup** dialog box
described below, by unchecking the **Show Profiler Values** check mark
(see also :numref:`fig:debug.prof-setup`).

.. rubric:: Profiler listing

If you are interested in a profiling overview comprising your entire
modeling application, you can get this through the **Profiler-Create
Listing File** menu. This will create a common source listing file of
your model text extended with profiling information wherever this is
available. Through the **Profiler Setup** dialog box described below you
can determine which profiler information will be added to the profiler
listing.

.. rubric:: Setting up profiling columns

For every new project, AIMMS uses a set of default settings to determine
which profiling information is displayed in the various available
methods to display profiling information. You can modify these settings
through the **Profiler-Setup** menu, which will open the **Profiler
Setup** dialog box illustrated in :numref:`fig:debug.prof-setup`.

.. figure:: profiler-setup-new.png
   :alt: The **Profiler Setup** dialog box
   :name: fig:debug.prof-setup

   The **Profiler Setup** dialog box

In this dialog box you can, on a project-wide basis, determine

-  which of the profiling display methods described are enabled (through
   the **Show Profiler Values** check mark), and

-  per such display method, which profiling information is to be
   displayed, their order, and their corresponding display format.

The settings selected in the **Profiler Setup** dialog box are saved
along within the project file, and will be restored when you reopen the
project in another AIMMS session.

.. rubric:: Pausing and continuing the profiler

Through the **Profiler-Pause** menu, you can temporarily halt the
gathering of profiling information by AIMMS, while the existing
profiling information will be retained. You can use this menu, for
example, when you only want to profile the core computational procedures
of your modeling application and do not want the profiling information
to be cluttered with profiling information that is the result of
procedure calls and definition evaluations in the end-user interface.
You can resume the gathering of profiling information through the
**Profiler- Continue** menu.

.. rubric:: Resetting the profiler

With the **Profiler-Reset** menu, you can completely reset all profiling
counters to zero. You can use this menu, if the profiling information of
your application has become cluttered. For instance, some procedures may
have been executed multiple times and, thus, disturb the typical
profiling times required by your entire application. After resetting the
profiling counters, you can continue to gather new profiling information
which will then be displayed in the various profiling displays.

.. rubric:: Exiting the profiler

You can completely disable the AIMMS profiler through the
**Profiler-Exit Profiler** menu. As a result, the gathering of profiling
information will be completely discontinued, and all profiling counters
will be reset to zero. Thus, when you restart the profiler, all
profiling information of a previous session will be lost.

.. _sec:debug.card:

Observing Identifier Cardinalities
----------------------------------

.. rubric:: Observing identifier cardinalities

Another possible cause of performance problems is when one or more
multi- dimensional identifiers in your model have missing or incorrectly
specified domain conditions. As a result, AIMMS could store far too much
data for such identifiers. In addition, computations with these
identifiers may consume a disproportional amount of time.

.. rubric:: Locating cardinality problems

To help you locate identifiers with missing or incorrectly specified
domain conditions, AIMMS offers the **Identifier Cardinalities** dialog
box illustrated in :numref:`fig:debug.id-card`. You can invoke it
through the **Tools-Diagnostic Tools-Identifier Cardinalities** menu.

.. figure:: identifier-card-new.png
   :alt: The **Identifier Cardinalities** dialog box
   :name: fig:debug.id-card

   The **Identifier Cardinalities** dialog box

.. rubric:: Available information

The **Identifier Cardinalities** dialog box displays the following
information for each identifier in your model:

-  the *cardinality* of the identifier, i.e. the total number of
   non-default values currently stored,

-  the *maximal cardinality*, i.e. the cardinality if all values would
   assume a non-default value,

-  the *density*, i.e. the cardinality as a percentage of the maximal
   cardinality,

-  the number of *active* values, i.e. of elements that lie within the
   domain of the identifier,

-  the number of *inactive* values, i.e. of elements that lie outside of
   the domain of the identifier, and

-  the *memory usage* of the identifier, i.e. the amount of memory
   needed to store the identifier data.

The list of identifier cardinalities can be sorted with respect to any
of these values.

.. rubric:: Locating dense data storage

You can locate potential dense data storage problems by sorting all
identifiers by their cardinality. Identifiers with a very high
cardinality and a high density can indicate a missing or incorrectly
specified domain condition. In most real-world applications, the
higher-dimensional identifiers usually have relatively few tuples, as
only a very small number of combinations have a meaningful
interpretation.

.. rubric:: Resolving dense storage

If your model contains one or more identifiers that appear to
demonstrate dense data storage, it is often possible to symbolicly
describe the appropriate domain of allowed tuple combinations. Adding
such a condition can be helpful to increase the performance of your
model by reducing both the memory usage and execution times.

.. rubric:: Locating inactive data

Another type of problem that you can locate with the **Identifier
Cardinalities** dialog box, is the occurrence of *inactive* data in your
model. Inactive data can be caused by

-  the removal of elements in one or more domain sets, or

-  data modifications in the identifier(s) involved in the domain
   restriction of the identifier.

.. rubric:: Problems with inactive data

In principle, inactive data does not directly influence the behavior of
your model, as the AIMMS execution engine itself will never consider
inactive data. Inactive data, however, can cause unexpected problems in
some specific situations.

-  One of the areas where you have to be aware about inactive data is in
   the AIMMS API (see also :ref:`lr:chap:api` of the Language Reference),
   where you have to decide whether or not you want the AIMMS API to
   pass inactive data to an external application or DLL.

-  Also, when inactive data becomes active again, the previous values
   are retained, which may or may not be what you intended.

As a result, the occurrence of inactive data in the **Identifier
Cardinalities** dialog box may make you rethink its consequences, and
may cause you to add statements to your model to remove the inactive
data explicitly (see also :ref:`lr:sec:data.control` of the Language
Reference).

.. |breakpoint-button-set| image:: breakpoint-button-set.png

.. |breakpoint-pointer| image:: breakpoint-pointer.png

.. |breakpoint-button-step-over| image:: breakpoint-button-step-over.png

.. |breakpoint-button-step-into| image:: breakpoint-button-step-into.png

.. |breakpoint-button-step-out| image:: breakpoint-button-step-out.png

.. |breakpoint-button-run-to-cursor| image:: breakpoint-button-run-to-cursor.png

.. |breakpoint-button-continue| image:: breakpoint-button-continue.png

.. |breakpoint-button-finish| image:: breakpoint-button-finish.png

.. |breakpoint-button-halt| image:: breakpoint-button-halt.png

.. |breakpoint-button-call-stack| image:: breakpoint-button-call-stack.png

.. |breakpoint-button-list| image:: breakpoint-button-list.png

.. |breakpoint-button-disable| image:: breakpoint-button-disable.png

.. |breakpoint-button-remove| image:: breakpoint-button-remove.png
