.. _chap:deploy:

Deploying End-User Applications
===============================

.. _deploying_end_user_applications:

.. rubric:: Deployment considerations

After a successful development phase of your AIMMS application, you have
to start thinking about its deployment. For the application to become a
successful end-user application too, you, as the application developer,
need to consider issues like *protecting your intellectual property*,
*authenticating your end-users*, and *distribution* of your application.
AIMMS offers various tools to help you in all of these areas.

.. rubric:: Application protection and authentication

AIMMS offers several levels of protection that can be applied to your
application. To protect the content of your model from being viewed by
the unauthorized users, AIMMS allows you to encrypt (parts of) your
model. To protect the application against unauthorized access you can
encrypt your application using the public keys of all authorized users.
To further arrange the appropriate level of access within an
organization, you can associate a user database with your AIMMS
application, which can then be used to authenticate individual users and
provide their level of access to your application. Protecting your AIMMS
application and authenticating its use are discussed in full detail in
:ref:`chap:security`.

.. rubric:: Application distribution

To be able to run an AIMMS project, your users will need a copy of the
project itself. To support easy project distribution, an AIMMS project
can be compacted and distributed as a single-file project. In addition,
your users need to have installed AIMMS on their computer. In order to
enter input data and/or run the model, end-users need a commercial
license of AIMMS.

.. rubric:: AIMMS PRO

A more convenient way to distribute your application to your end-users
is to use the AIMMS *P*\ ublishing and *R*\ emote *O*\ ptimization
platform. AIMMS PRO makes it possible to deploy AIMMS applications to
end-users quickly and efficiently through the AIMMS PRO Portal. More
importantly, the AIMMS PRO Portal assures end-users can access the
latest version of these AIMMS applications (and the corresponding
versions of the AIMMS software) at all times through a web browser. More
information on AIMMS PRO can be found in the AIMMS PRO `User's Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__.

.. rubric:: This chapter

This chapter gives some background on AIMMS end-user mode, and discusses
.aimmspack project files, which you can use to distribute your AIMMS
project as a single file.

.. _sec:deploy.end-user:

Running End-User Applications
-----------------------------

.. rubric:: Running end-user projects
   :name: end-user-app

An AIMMS project can run in two different modes, *developer* mode and
*end-user* mode. While the developer mode allows you to use the full
functionality described in this `User's Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__, the end-user mode only
allows you to *access* the end-user pages of the AIMMS project that were
created in developer mode.

.. rubric:: Disabled functionality

The AIMMS end-user mode lacks the essential tools for creating and
modifying model-based applications. More specifically, the following
tools are not available in end-user mode:

-  the **Model Explorer**,

-  the **Identifier Selector**,

-  the **Page Manager**,

-  the **Template Manager**, and

-  the **Menu Builder** tool.

Thus, in end-user mode, there is no way in which an end-user can modify
the contents of your AIMMS-based application.

.. rubric:: Allowed usage

AIMMS end-users can only perform tasks specified by you as an
application developer. Such tasks must be performed through data
objects, buttons and the standard, or custom, end-user menus associated
with the end-user pages in your project. They include:

-  modifying the input data for your model in the end-user interface,

-  executing procedures within your model to read data from an external
   data source, or performing a computation or optimization step,

-  viewing model results in the end-user interface,

-  writing model results to external data sources or in the form of
   printed reports, and

-  performing case management tasks within the given framework of case
   types.

Thus, an end-user of your application does not need to acquire any
AIMMS- specific knowledge. The only requirement is that the interface
that you have created around your application is sufficiently intuitive
and clear.

.. rubric:: Requirements

Before you can distribute your AIMMS project as an end-user application,
two requirements have to be fulfilled:

-  you must have exported your application as an *end-user project* (see
   :ref:`sec:security.encrypt`), and

-  you need to associate a *startup page* with your application which
   will be displayed when your application is started by an end-user.

.. rubric:: Assigning a startup page

For every end-user project, you must associate a single page within the
project so that it becomes the project's *startup page*. Such an
association can either be made directly by selecting a page for the
'Startup Page' option in the AIMMS **Options** dialog box (see
:ref:`sec:setting.options`), or implicitly as the first opened page in
the *startup procedure* of the project using a call to the :any:`PageOpen`
function.

.. rubric:: Role of startup page

After opening your project in end-user mode, AIMMS will display the
startup page. As all communication between the end-user and your model
is conducted through end-user pages of your design, this first page
and/or its menus must provide access to all the other parts of your
AIMMS application that are relevant for your end-users. If all pages are
closed during a session, the end- user can still re-open the startup
page using the first page button |firstpage| on the **Project** toolbar,
or via the **View-First Page** menu.

.. rubric:: Startup procedure

In addition to a startup page you can also provide a startup procedure
in the project-related AIMMS options. Inside the startup procedure you
can perform any initializations necessary for an end-user to start
working with the project. Such initializations can include setting up
date or user related aspects of the project, or reading the data for
particular identifiers from a database.

.. rubric:: Replacing the splash screen

By default, AIMMS will display a splash screen during startup. When you
are opening AIMMS with a particular project, you can replace AIMMS' own
splash screen with a bitmap of your choice. If the project directory
contains a bitmap (``.bmp``) file with the same name as the project
file, AIMMS will display this bitmap file on the splash screen. In such
a bitmap you can display, for instance, version information about your
project.

.. _sec:deploy.aimmspack:

Preparing an AIMMS Application for Distribution
-----------------------------------------------

.. rubric:: .aimmspack project files...

A complete AIMMS application consists of several files (see
:ref:`sec:start.files` for an overview of these files), all of which
should also be distributed to the user of the application. To make the
distribution of an AIMMS application easier, AIMMS offers the
possibility to distribute your project as a single-file project, by
packing all relevant files into a single file with the
.aimmspack extension.

.. rubric:: Creating a .aimmspack file

Turning your project into a single-file, end-user project is very
straightforward. Just select the **File-Export End User Project** menu
command which will open the **Select destination .aimmspack File**
dialog box which allows you to specify the name and location of the file
to be exported.

.. rubric:: Encryption

After having specified a destination file, the **Encryption of Exported
End-User Project** dialog box opens which allows you the security
setting for your end-user project. Encryption is described in more
detail in :ref:`sec:security.encrypt`.

.. rubric:: The .aimmspack file contents

By default AIMMS will select all files in the project directory to be
included in the .aimmspack file while ignoring all files in the
``Backup`` and :any:`Log` directories. All files associated with referenced
library projects will also be included. The project (``.aimms``) and all
source files (e.g. ``.ams`` and ``.xml`` files) for the main project and
all of the library projects involved are mandatory while the settings
for all other files can be changed through the **Select Files for
Export** dialog box (see :numref:`fig:deploy.export-select`. Note that
only include files (and folders) that are contained in the main project
folder and folders of library projects. If your project refers to files
in other locations you must make sure that these files exist on the
destination computer.

.. figure:: zipprj-select-new.png
   :alt: The **Select Files for Export** dialog box
   :name: fig:deploy.export-select

   The **Select Files for Export** dialog box

.. rubric:: Running an .aimmspack project file

The .aimmspack project files are dealt with as if they were ordinary
AIMMS project files. Both the developer and end-user version of AIMMS
are capable of running both ``.aimms`` and .aimmspack files.

.. rubric:: Unpacking an .aimmspack project file :math:`\ldots`

When a *developer* opens an .aimmspack project file, he will *always* be
prompted for a location where to unpack to. On the other hand, when an
*end-user* opens an .aimmspack project file, only the *first time*, he
will be prompted for a location where to unpack to. Any subsequent time
an end-user opens the .aimmspack file, AIMMS will look whether the
location where the .aimmspack file was unpacked previously contains the
unpacked version of the same .aimmspack file. If so, AIMMS will open the
unpacked version without user interaction. Otherwise, AIMMS will prompt
the end-user for a (new) location, unpack the .aimmspack project and run
it with AIMMS. So, when you send your end-user an updated version of a
packed project, AIMMS will notice the version change and prompt the
end-user with a question whether or not to overwrite the existing
project with the new version.

.. rubric:: :math:`\ldots` using the command line

Alternatively, you can indicate where you want the .aimmspack file to be
unpacked via the commandline argument *\--unpack-folder*.

.. rubric:: Selecting an AIMMS version

If multiple AIMMS versions have been installed on the computer, you can
select the particular AIMMS version to use on an ``.aimms`` or
.aimmspack file through the right-mouse popup menu in Windows Explorer.
If you double-click an ``.aimms`` or .aimmspack the ``AimmsLauncher``
utility program will try to open the AIMMS version that most closely
matches the AIMMS version that was used to create the project.

.. |firstpage| image:: firstpage.png
