.. _chap:setting:

Project Settings and Options
============================

.. rubric:: This chapter

Several aspects of AIMMS, including its startup behavior, its
appearance, the inner workings of the AIMMS execution engine or the
solvers used in a session, can be customized to meet the requirements of
your project. This chapter describes the various tools available in
AIMMS for making such customizations.

.. _sec:setting.options:

AIMMS Execution Options
-----------------------

.. rubric:: Options

Many aspects of the way in which AIMMS behaves during a session can be
customized through the AIMMS execution *options*. Such options can be
set either globally through the options dialog box, or from within the
model using the ``OPTION`` statement. As every project has its own
requirements regarding AIMMS' behavior, option settings are stored per
project in the project file.

.. rubric:: Option types

AIMMS offers options for several aspects of its behavior. Globally, the
AIMMS execution options can be categorized as follows.

Project options
   how does AIMMS behave during startup, and how does AIMMS appear
   during a project.

Execution options
   how does the AIMMS execution engine with respect to numeric
   tolerances, reporting, case management and various other execution
   aspects.

General solver options
   how does AIMMS behave during the matrix generation process, and which
   information is listed.

Specific solver options
   how are the specific solvers configured that are used in the project.

.. rubric:: Option dialog box

Through the **Settings-Project Options** menu you can open the global
AIMMS **Options** dialog box illustrated in
:numref:`fig:setting.options`.

.. figure:: options-new.png
   :alt: The AIMMS **Options** dialog box
   :name: fig:setting.options

   The AIMMS **Options** dialog box

In this dialog box, an *option tree* lists all available AIMMS execution
and solver options in a hierarchical fashion.

.. rubric:: Modifying options

After selecting an option category from the left-hand side of the
**Options** dialog box, you can modify the values of the options in that
category on the right-hand side of the dialog box. As illustrated in
:numref:`fig:setting.options`, AIMMS lists the currently selected value
for every option (in the first edit field) along with the allowable
range of all possible option values (in the second field). Option values
can be either integer numbers, floating point numbers or strings, and,
depending on the option, you can modify its value through

-  a simple edit field,

-  radio buttons,

-  a drop-down list, or

-  a wizard in the case where the value of an option is model-related.

.. rubric:: Committing options

With the **Apply** button, you can commit the changes you have made to
the value of a particular option and continue changing other options;
the **OK** button will commit the changes and close the option dialog
box. With the **Default** button at the right-hand side of the dialog
box, you can always reset the option to its default value. It is only
active when the option has a nondefault value.

.. rubric:: Option description

When you have selected an option, and need to know more about its
precise meaning before changing its value, you can press the **Help**
button at the right-hand side of the options dialog box. As illustrated
in :numref:`fig:setting.option-help`,

.. figure:: opt-help-new.png
   :alt: Option help
   :name: fig:setting.option-help

   Option help

this will open a help window containing a more detailed description of
the selected option.

.. rubric:: Options with nondefault value

To help you quickly identify all the options which you have modified for
a particular project, all modified options are summarized at the end of
the options tree in a special section, **Options with nondefault
value**. You can modify these options either in this section, or in
their original locations. If you set a modified option back to its
default value, it will be removed from the nondefault section. When you
select an option from the **Options with nondefault value** section, the
|locate| **Location in Tree** button will become available. Pressing
this button will select the originating option category in the option
tree.

.. rubric:: Copying solver options

When you add a new version of some solver to the solver configuration
(see :ref:`sec:setting.solver` for a description of how to add a new
solver), the options of this new solver will appear in the **Specific
Solvers** category. To copy solver options from the old solver version
(e.g. Cplex 11.1 to CPLEX 12.6), select the source solver in the option
tree and select the **Copy Option** command from the right-mouse popup
menu. This will open the **Copy Options** dialog box as shown in
:numref:`fig:setting.copy-options`.

.. figure:: copy-options-new.png
   :alt: The **Copy Options** dialog box
   :name: fig:setting.copy-options

   The **Copy Options** dialog box

By default this dialog will only show options that differ between both
solvers plus options that are only available in one of the two solvers.
Once you press the **Ok** button, all options that remain in this list
(and are available in both solvers) are copied from the source to the
destination solver.

.. rubric:: Searching for options

When you know (part of) the name of an option, but do not know where it
is located in the option tree, you can use the search facility in the
lower left- hand part of the option dialog box to help you find it. When
you enter (part of) an option name, AIMMS will jump to the first option
in the tree whose name contains the entered string.

.. rubric:: Setting options within the model

In addition to modifying option values in the options dialog box, you
can also set options from within your model using the ``OPTION``
statement. The ``OPTION`` statement is discussed in the AIMMS Language
Reference. While changes to option values in the options dialog box are
stored in the project file and reused at the beginning of the next
project session, run time option settings are lost when you close the
project. Setting options during run time can be convenient, however, if
different parts of your model need different option settings.

[sec:setting.project]

.. rubric:: Setting up an end-user project

A number of options and settings are of particular importance when you
want to set up a project in such a manner that it is ready to be used by
end-users. You can find these options in the **Project-Startup &
authorization** and the **Project-Appearance** sections of the
**Options** dialog box. This section discusses the most important
options.

.. rubric:: Startup procedure

With the *startup procedure* option you can select a procedure within
your model which you want to be executed during the start up of your
project. Such a procedure can perform, for instance, all the necessary
data initialization for a proper initial display of the end-user GUI
automatically, thus preventing your end-users from having to perform
such an initialization step themselves.

.. rubric:: Startup page

With the *startup page* option, you can indicate the page which AIMMS
will display at start up. It is important to specify a startup page for
end-user projects, as all data communication with the model must take
place through end- user pages designed by you. Therefore, you should
also ensure that every relevant part of your application can be reached
through the startup page.

.. rubric:: Startup by-pass

In a developer project you can by-pass the startup sequence by holding
down the Shift key when you select the project to be opened.

.. rubric:: Project title

By default, AIMMS will display the name of the currently loaded project
in the title bar of the AIMMS window. Using the *project title* option
you can modify this title, for instance to provide a longer description
of your project.

.. _sec:setting.solver:

Solver Configuration
--------------------

.. rubric:: Configuring solvers

With every AIMMS system you can obtain a license to use particular
solvers to solve mathematical programs of a specific type. As AIMMS
provides a standardized interface to its solvers, it is even possible
for you to link your own solver to AIMMS. This section provides an
overview of how to add solvers to your system or modify the existing
solver configuration.

.. rubric:: Solver configuration dialog box

You can obtain a list of solvers currently known to your AIMMS system
through the **Settings-Solver Configuration** menu. This will open the
**Solver Configuration** dialog box illustrated in
:numref:`fig:setting.solver-config`.

.. figure:: slv-cfg-new.png
   :alt: The **Solver Configuration** dialog box
   :name: fig:setting.solver-config

   The **Solver Configuration** dialog box

The dialog box shows an incidence matrix between all available solver
and types of mathematical programs. An 'x' indicates the capability of a
specific solver to solve mathematical programs of a particular type. A
bold '**X**' indicates that the specific solver is used as the default
solver for mathematical problems of a particular type.

.. rubric:: Modifying solver settings

The buttons on the right-hand side of the dialog box let you globally
modify the solver configuration of your AIMMS system. Through these
buttons you can perform tasks such as:

-  modify the default solver for a particular model type, and

-  add or delete solvers.

.. rubric:: Selecting default solver

With the **Set Default** button you can set the default solver for a
particular type of mathematical program. AIMMS always uses the default
solver when solving a mathematical program of a particular type. A run
time error will occur, if you have not specified an appropriate solver.

.. rubric:: Adding a solver

When you want to add an additional solver to your system, you can select
the **Add** button from the **Solver Configuration** dialog box,
respectively. This will open a **Solver Configuration Data** dialog box
as shown in :numref:`fig:setting.solver-data`.

.. figure:: slv-data-new.png
   :alt: The **Solver Configuration Data** dialog box
   :name: fig:setting.solver-data

   The **Solver Configuration Data** dialog box

In this dialog box you have an overview of the interface DLL, the name
by which the solver is known to AIMMS and any appropriate arguments that
may be needed by the solver.

.. rubric:: Select solver DLL

In the **Solver DLL** area of the **Solver Configuration Data** dialog
box you can select the DLL which provides the interface to the solver
that you want to link to AIMMS. AIMMS determines whether the DLL you
selected is a valid solver DLL, and, if so, automatically adds the
solver name stored in the DLL to the **Description** field.

.. rubric:: Solver arguments

In the **Arguments** area of the **Solver Configuration Data** dialog
box you can enter a string containing solver-specific arguments. You may
need such arguments, for instance, when you have a special licensing
arrangement with the supplier of the solver. For information about which
arguments are accepted by specific solvers, please refer to the help
file accompanying each solver.

.. rubric:: Installation automatically adds

After you install a new AIMMS version, AIMMS will automatically add the
solvers available in that installation to the **Solver Configuration**
dialog box. If the newly installed solver is the first solver of a
particular type, AIMMS will also automatically make the solver the
default solver for that type. Thus, after installing a new AIMMS system,
you do not have to worry about configuring the solvers in most cases,
provided of course that your AIMMS license permits the use of the
solvers you have installed.

.. rubric:: Using a nondefault solver

By modifying the value of the predefined element parameter
:any:`CurrentSolver` in the predefined :any:`AllSolvers` during run time you
can, at any time during the execution of your model, select a nondefault
solver for a given mathematical programming type that you want AIMMS to
use during the next ``SOLVE`` statement for a mathematical program of
that type. At startup, AIMMS will set ``CurrentLPSolver`` to the default
LP solver as selected in the solver configuration dialog box.

.. _sec:setting.print:

Print Configuration
-------------------

.. rubric:: Print configuration

AIMMS offers two distinct facilities to create printed reports
associated with your model, namely printouts of graphical end-user pages
and print pages (see :ref:`chap:print`), and printouts of text files
such as a text representation of a part of the model tree or the
listing, log and ``PUT`` files. This section explains how you can
configure the printing properties for both types of reports.

.. rubric:: Printing end-user pages

End-user pages and print pages are printed according to the settings
that you have selected for these pages. These settings include:

-  the selection of the paper type on which pages are printed (see
   :ref:`sec:print.pages`), and

-  the selection of object fonts and colors through the AIMMS font and
   color selection dialog boxes (see :ref:`sec:prop.property`).

These settings must be fixed by you as the application developer, and
cannot be changed by an end-user of your application. An end-user can,
however, still select the printer to which the output must be sent, as
explained below.

.. rubric:: Text printing

Text files can be printed from within AIMMS, either from the
**File-Print** menu inside an AIMMS text editor window, or through a
call to the :any:`FilePrint` procedure from within a procedure in your
model. The print properties of all text files that you want to print, in
either manner, can be modified through the **Settings-Text Printing**
menu. This will invoke the dialog box illustrated in
:numref:`fig:setting.text-print`.

.. figure:: text-prn-new.png
   :alt: The **Text Printing** dialog box
   :name: fig:setting.text-print

   The **Text Printing** dialog box

.. rubric:: Text printing properties

In the **Text Printing** dialog box you can select the paper type and
font with which you want all text files to be printed. For the paper
type you can select one of the predefined paper types, or specify a user
defined paper type by providing the page height and width, as well as
the margins on each side of the page. By pressing the **Font** button on
the right-hand side of the dialog box, you can select the font with
which you want your text files to be printed. The text printing
properties are stored globally on your machine.

.. rubric:: Printer setup

With the **File-Print Setup** menu you can select the printer on which
print pages and text files associated with your project are printed, and
modify the properties of that printer. This command will invoke the
standard Windows **Print Setup** dialog box illustrated in
:numref:`fig:setting.print-setup`.

.. container:: center

   .. figure:: prn-cfg-new.png
      :alt: The **Print Setup** dialog box
      :name: fig:setting.print-setup

      The **Print Setup** dialog box

.. rubric:: Default settings

The settings selected in this dialog box will only be valid during the
current session of AIMMS. If you want to modify the default print setup
globally, you can do this through the **Printer** section in the Windows
**Control Panel**. There you can

-  select a **Default** printer from the list of all printers available
   on your system, and

-  modify the **Document Defaults** (i.e. the printer settings with
   which each print job is printed by default) for every individual
   printer on your system.

Without a call to the **File-Print Setup** dialog box, AIMMS will use
the default printer selected here, and print according to the document
defaults of that printer.

.. |locate| image:: locate.png
