.. _chap:view:

Viewing Identifier Selections
=============================

.. rubric:: Identifier overviews

Although the **Model Explorer** is a very convenient tool to organize
all the information in your model, it does not allow you to obtain a
simultaneous overview of a group of identifiers that share certain
aspects of your model. By mutual comparison of important attributes
(such as the definition), such overviews may help you to further
structure and edit the contents of your model, or to discover oversights
in a formulation.

.. rubric:: This chapter

To assist you in creating overviews that can help you analyze the
interrelationships between identifiers in your model, AIMMS offers the
**Identifier Selector** tool and **View** windows. This chapter helps
you understand how to create meaningful identifier selections with the
**Identifier Selector**, and how to display such selections using
different views.

.. _sec:view.selection:

Creating Identifier Selections
------------------------------

.. rubric:: Select by similarity
   :name: identifier-selector

When you are developing or managing a large and complicated model, you
sometimes may need an overview of all identifiers that have some sort of
similarity. For example, it may be important to have a simultaneous view
of

-  all the constraints in a model,

-  all variables with a definition,

-  all parameters using a certain domain index, or

-  all identifiers that cover a specific part of your model.

.. rubric:: Identifier selections

In AIMMS, you can create a list of such identifiers using the
configurable **Identifier Selector** tool. This tool helps you to create
a selection of identifiers according to a set of one or more criteria of
varying natures. You can let AIMMS create a once only selection directly
in the **Model Explorer**, or create a compound selection in the
**Identifier Selector**, which allows you to intersect or unite multiple
selections.

.. rubric:: Creating once only selections

If you need a selection only once, then you can create it directly in
the **Model Explorer** by

-  either manually selecting one or more nodes in the tree, or

-  using the **View-Selection** menu to create a custom selection based
   on one or more of the conditional selection criteria offered by AIMMS
   (explained below).

In both cases, the resulting list of selected identifiers will be
highlighted in the model tree. If you like, you can narrow down or
extend the selection by applying one or more subsequent conditional
selections to the existing selection.

.. rubric:: The Identifier Selector

If you need a specific selection more than once, then you can create it
in the **Identifier Selector** tool. The **Identifier Selector**
consists of a tree in which each node contains one of the three types of
identifier selectors described below. :numref:`fig:view.sel-tree`
illustrates an example selector tree.

.. figure:: sel-man-new.png
   :alt: The selector tree
   :name: fig:view.sel-tree

   The selector tree
   
.. rubric:: Selector types

In the **Identifier Selector** tool, you can add nodes corresponding to
several types of identifier selectors:

-  a *node-based selector* , where all the identifiers below one or more
   user-selected nodes in the model tree are added to the selection,

-  a *conditional selector* , where the list of identifiers is created
   dynamically on identifier type and/or the contents of one of their
   respective attributes,

-  a *set-dependent selector* , where the list of identifiers is created
   dynamically based on a specific set in either the domain or range of
   identifiers, or

-  a *type-based selector* , where the list of identifiers consists of
   all variables of a certain type (e.g. free, nonnegative, binary) or
   all constraints of a certain type (:math:`\leq`, :math:`=` or
   :math:`\geq`). This selector can only be used in combination with the
   Math Program Inspector.

While the above four selectors allow you to define selections based on a
symbolic criteria, the four types of identifier selectors below allow
you to specify selections based on individual criteria. The main purpose
of these selectors is to define selections that can be used in the Math
Program Inspector (see :ref:`chap:mpinspector`).

-  an *element-dependent selector* , where the list of individual
   identifiers is created dynamically based of the occurrence of one or
   more specific elements in the domain,

-  a *scale-based selector* , where the list of identifiers is built up
   from all variables and constraints for which the ratio between the
   largest absolute value and the smallest absolute value in the
   corresponding row or column of the matrix exceeds a given value,

-  a *status-based selector* , where the list of identifiers is built up
   from all variables and constraints for which the solution satisfies
   some property (e.g. feasible, basic, at bound), or

-  a *value-based selector* , where the list of identifiers is built up
   from all variables and constraints for which the level, bound,
   marginal, or bound violation value satisfy satisfy some property.

Through the **View-Selection** menu in the **Model Explorer** you can
only create a new, or refine an existing, selection using a *conditional
selector*.

.. rubric:: Selection dialog box

To create a selector, AIMMS offers special dialog boxes which let you
specify the criteria on which to select. As an example the dialog box
for creating a conditional selector is illustrated in
:numref:`fig:view.cond-sel`.

.. figure:: cond-sel-new.png
   :alt: The **Conditional Selector** dialog box
   :name: fig:view.cond-sel

   The **Conditional Selector** dialog box

In it, you can select (by double clicking) one or more identifier types
that you want to be part of the selection and filter on specific
attributes that should be either empty, nonempty, or should contain a
particular string.

.. rubric:: Compound selections

The tree structure in the **Identifier Selector** defines combinations
of selectors by applying one of the set operators *union*, *difference*
or *intersection* with respect to the identifier selection represented
by the parent node. The root of the tree always consists of the fixed
selection of all model identifiers. For each subsequent child node you
have to indicate whether the node should add identifiers to the parent
selection, should remove identifiers from the parent selection, or
should consider the intersection of the identifiers associated with the
current and the parent selection. Thus, you can quickly compose
identifier selections that satisfy multiple selection criteria. The type
of set operation applied is indicated by the icon of each node in the
identifier selector.

.. rubric:: Refining model tree selections

In the **Model Explorer**, the *union*, *difference* and *intersection*
operations apply to the identifier selection that is currently
highlighted in the model tree. You can use them to add identifiers to
the current selection, to remove identifiers from the current selection,
or filter the current selection by means of an additional criterion.

.. rubric:: Using selections

The list of identifiers that results from a (compound) identifier
selector can be used in one of the following ways:

-  you can display the identifiers in a **View** window of your choice
   (explained in the next section),

-  you can restrict the set of variables and constraints initially
   displayed in the **Math Program Inspector** (see
   :ref:`chap:mpinspector`), or

-  by dragging and dropping a selector into the **Model Explorer**, the
   corresponding identifiers will be highlighted in the model tree.

.. rubric:: Advanced drag and drop

The drag-and-drop features of AIMMS make it very easy to fill a **View**
window with identifiers from either the model tree, the **Identifier
Selector** or other **View** windows. If you drag-and-drop a selection
into any other AIMMS window, AIMMS will interpret this as a special
search action to highlight all occurrences of the selected identifiers
as follows:

-  in the *model tree* all identifiers in the selection will be
   highlighted,

-  in the *page* or *template tree* all pages that contain reference to
   the identifiers in the selection will be highlighted,

-  in an end-user *page*, in edit mode, all objects that contain
   references to the identifiers will be selected, and

-  in the *menu builder tree*, AIMMS will highlight all menu items that
   reference one or more identifiers in the selection.

In addition, AIMMS also supports the 'drag-and-drop-search' action in a
**View** window by pressing both the **Shift** and **Control** key
during the drop operation.

.. _sec:view.view:

Viewing Identifier Selections
-----------------------------

.. rubric:: Overview of attributes
   :name: view-ident-select

After you have created an identifier selection, in either the **Model
Explorer** or in the **Identifier Selector**, you may want to compare or
simultaneously edit multiple attributes of the identifiers in the
selection. In general, sequential or simultaneous, opening of all the
corresponding single attribute forms is impractical or unacceptable for
such a task. To assist, AIMMS offers special identifier **View**
windows.

.. rubric:: Identifier views

A **View** window allows you to view one or more attributes
simultaneously for a number of identifiers. Such a **View** window is
presented in the form of a table, where each row represents a single
identifier and each column corresponds to a specific attribute. The
first column is always reserved for the identifier name. An example of
an identifier **View** window is given in :numref:`fig:view.view`.

.. figure:: view-win-new.png
   :alt: Example of a View window
   :name: fig:view.view

   Example of a View window

.. rubric:: Editing in a View window

In addition to simply viewing the identifier content in a **View**
window, you can also use it to edit individual entries. To edit a
particular attribute of an identifier you can just click on the relevant
position in the **View** window and modify the attribute value. This can
be convenient, for instance, when you want to add descriptive text to
all identifiers for which no text has yet been provided, or when you
want to make consistent changes to units for a particular selection of
identifiers. As in a single attribute form, the changes that you make
are not committed in the model source until you use one of the special
compile buttons at the top right of the window (see also
:ref:`sec:decl.commit`).

.. rubric:: Opening a View window

Using the **Edit-Open with** menu, or the **Open with** item in the
right- mouse pop-up menu, you can open a particular **View** window for
any identifier selection in the model explorer or in the identifier
selector. Selecting the **Open with** menu will open the **View
Manager** dialog box as displayed in :numref:`fig:view.dialog`.

.. figure:: view-sel-new.png
   :alt: The **View Manager** dialog box
   :name: fig:view.dialog

   The **View Manager** dialog box

In the **View Manager** you must select one of the available *view
window definitions*, with which to view the given identifier selection.
For every new project, the **View Manager** will automatically contain a
number of basic view window definitions that can be used to display the
most common combinations of identifier attributes.

.. rubric:: Creating a view window definition

Using the **Add**, **Delete** and **Properties** buttons in the **View
Manager**, you can add or delete view window definitions to the list of
available definitions, or modify the contents of existing definitions.
For every view window definition that you add to the list or want to
modify, AIMMS will open the **View Definition Properties** dialog box as
illustrated in :numref:`fig:view.view-prop`.

.. figure:: view-prp-new.png
   :alt: **View Definition Properties** dialog box
   :name: fig:view.view-prop

   **View Definition Properties** dialog box

With this dialog box you can add or remove attributes from the list of
attributes that will be shown in the **View** window, or change the
order in which the particular attributes are shown.

.. rubric:: Changing the View window contents

After opening a **View** window, with the contents of a particular
identifier selection, you can add new identifiers to it by dragging and
dropping other identifier selections from either the **Model Explorer**
or the **Identifier Selector**. Using the **Edit-Delete** menu or the
**Del** key, on the other hand, you can delete any subselection of
identifiers from the **View** window. At any time you can save the
modified identifier selection as a new node in the identifier selector
tree through the **View-Selection-Save** menu.

.. rubric:: Selecting identifier groups

Besides selecting individual identifiers from the model tree, you can
also select whole groups of identifiers by selecting their parent node.
For example, if you drag-and-drop an entire declaration section into a
**View** window, all the identifiers contained in that section will be
added to the view.

.. rubric:: Specifying a default view

As can be seen at the bottom of the **View Manager** dialog box in
:numref:`fig:view.dialog`, it is possible to associate a default view
definition with every selector in the **Identifier Selector**. As a
consequence, whenever you double-click on such an identifier selector
node, AIMMS will immediately open a default **View** window with the
current contents of that selection.
