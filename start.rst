.. _chap:start:

Getting Started
===============

.. rubric:: This chapter

This chapter provides pointers to the various AIMMS examples that are
available online and may help you to get a quick feel for the system. It
explains the principle steps involved in creating a new AIMMS project,
and it provides you with an overview of the various graphical tools
available in AIMMS to help you create a complete Analytic Decision
Support application. In addition, the chapter discusses the files
related to an AIMMS project.

.. _sec:start:demos:

Getting Started with AIMMS
--------------------------

.. rubric:: Learn by example

For most people, learning to use a new software tool like AIMMS is made
substantially easier by first getting to see a few examples of its use.
In this way you can get a quick feel for the AIMMS system, and begin to
form a mental picture of its functionality.

.. rubric:: Getting started quickly

In addition, by taking one or more illustrative examples as a starting
point, you are able to quickly create simple but meaningful AIMMS
projects on your own, without having to read a large amount of
documentation. Building such small projects will further enhance your
understanding of the AIMMS system, both in its use and in its
capabilities.

.. rubric:: AIMMS tutorial

To get you on your way as quickly as possible, the AIMMS system comes
with a tutorial consisting of

-  a number of live demos illustrating the basic use of AIMMS,

-  an extensive library of small example projects each of which
   demonstrates a particular component of either the AIMMS language or
   the end-user interface,

-  a number of complete AIMMS applications, and

-  worked examples corresponding to chapters in the book on optimization
   modeling.

.. rubric:: Example projects

The library of small example projects deals with common tasks such as

-  creating a new project,

-  building a model with your project,

-  data entry,

-  visualizing the results of your model,

-  case management, and

-  various tools, tips and tricks that help you to increase your
   productivity.

.. rubric:: What you learn

By quickly browsing through these examples, you will get a good
understanding of the paradigms underlying the AIMMS technology, and you
will learn the basic steps that are necessary to create a simple, but
fully functional modeling application.

.. rubric:: This `User's Guide <https://documentation.aimms.com/_downloads/AIMMS_user.pdf>`__

Rather than providing an introduction to the use of AIMMS, the User's
Guide deals, in a linear and fairly detailed fashion, with all relevant
aspects of the use of AIMMS and its modeling tools that are necessary to
create a complete modeling application. This information enables you to
use AIMMS to its full potential, and provides answers to questions that
go beyond the scope of the example projects.

.. rubric:: The Language Reference

The Language Reference deals with every aspect of AIMMS data types and
the modeling language. You may need this information to complete the
attribute forms while adding new identifiers to your model, or when you
specify the bodies of procedures and functions in your model.

.. rubric:: The Optimization Modeling guide

The Optimization Modeling guide provides you with the basic principles
of optimization modeling, and also discusses several advanced
optimization techniques which you may find useful when trying to
accomplish nontrivial optimization tasks.

.. rubric:: How to proceed

The following strategy may help you to use AIMMS as efficiently and
quickly as possible.

-  Study some of the working examples to get a good feel for the AIMMS
   system.

-  Select an example project that is close to what you wish to achieve,
   and take it as a starting point for your first modeling project.

-  Consult any of the three AIMMS books whenever you need more thorough
   information about either the use of AIMMS, its language or tips on
   optimization modeling.

.. _sec:start.new:

Creating a New Project
----------------------

.. rubric:: Project components
   :name: new-project

Every AIMMS application consists of two main components:

-  an AIMMS project file (with a ``.aimms`` extension), which contains
   references to the main application project and all library projects
   contained in your application,

-  for the main project and every library project all source files are
   stored in a separate folder:

   -  the ``Project.xml`` file holding a reference to the project's main
      model source file (with an ``.ams`` extension), as well as all
      additional model source files included in the main model source
      file, together containing all identifier declarations, procedures
      and functions that are relevant to the project,

   -  ``PageManager.xml``, ``TemplateManager.xml`` and
      ``MenuBuilder.xml`` files describing the page, template and menu
      tree defined in the project, with all individual pages and
      templates being stored in the ``Pages`` and ``Templates``
      subfolder,

   -  ``Settings`` and ``Tools`` subfolders containing the options for
      the execution engine, and the saved settings for user colors,
      fonts, and the various tools in the AIMMS IDE, and

   -  the ``User Files`` folder for storing all user files that you
      store within the project.

.. rubric:: Creating a new project

Within an AIMMS session, you can create a new project through the
**File-New Project** menu. Note that this menu is only available when no
other project is currently open. It will open the AIMMS **New Project**
wizard illustrated in :numref:`fig:start.new-project`.

.. figure:: new-prj-new.png
   :alt: The AIMMS **New Project** wizard
   :name: fig:start.new-project

   The AIMMS **New Project** wizard

In this wizard you can enter the name of the new project, along with the
directory in which the project is to be stored, and the model file (with
the ``.ams`` extension) to be associated with the project.

.. rubric:: Project directory

By default, the AIMMS **New Project** wizard suggests that the new
project be created in a new subdirectory with the same name as the
project itself. You can use the wizard button |wizard| to the right
of the location field to modify the location in which the new project is
created. However, as AIMMS creates a number of additional files and
directories in executing a project, you are strongly advised to store
each AIMMS project in a separate directory.

.. rubric:: Model file

By default, the AIMMS **New Project** wizard assumes that you want to
create a new model file with the same name as the project file (but with
a different extension). You can modify the name suggested by the wizard
to another existing or nonexisting model file. If the model associated
with the project does not yet exist, it will be automatically created by
AIMMS.

.. rubric:: The Model Explorer

After you have finished with the **New Project** wizard, AIMMS will open
the **Model Explorer**, an example of which is illustrated in
:numref:`fig:start-model-explorer`.

.. figure:: start-model-explorer.png
   :alt: The AIMMS Model Explorer
   :name: fig:start-model-explorer

   The AIMMS Model Explorer

The Model Explorer is the main tool in AIMMS to build an AIMMS model,
the starting point of building any AIMMS application. In the Model
Explorer, the model is presented as a tree of identifier declarations,
allowing you to organize your model in a logical manner and make it
easy-both for you and others who have to inspect your model-to find
their way around. Besides the Model Explorer, AIMMS provides a number of
other development tools for model building, GUI building and data
management. An overview of these tools is given in
:ref:`sec:start.tools`.

.. rubric:: Starting an existing project

You can open an existing AIMMS project in two ways. You can either

-  start AIMMS and open the project via the **File-Open Project** menu,
   or

-  double click on the AIMMS project file (with a ``.aimms`` extension)
   in Windows Explorer.

After opening a project, AIMMS may take further actions (such as
automatically opening pages or executing procedures) according to the
previously stored project settings.

.. _sec:start.tools:

Modeling Tools
--------------

.. rubric:: Modeling tools

Once you have created a new project and associated a model file with it,
AIMMS offers a number of graphical tree-based tools to help you further
develop the model and its associated end-user interface. The available
tools are:

-  the *Model Explorer*,

-  the *Identifier Selector*,

-  the *Page Manager*,

-  the *Template Manager*, and

-  the *Menu Builder* tool.

These tools can be accessed either through the **Tools** menu or via the
project toolbar. They are all aimed at reducing the amount of work
involved in developing, modifying and maintaining particular aspects of
your model-based end-user application.
:numref:`fig:start.tools-overview` provides an overview of the windows
associated with each of these tools.

.. figure:: start-overview.png
   :alt: Overview of Aimms tools
   :name: fig:start.tools-overview

   Overview of Aimms tools


.. rubric:: The Model Explorer

The AIMMS **Model Explorer** provides you with a simple graphical
representation of all the identifiers, procedures and functions in your
model. All relevant information is stored in the form of a tree, which
can be subdivided into named sections to store pieces of similar
information in a directory-like structure. The leaf nodes of the tree
contain the actual declarations and the procedure and function bodies
that make up the core of your modeling application. The **Model
Explorer** is discussed in full detail in :ref:`chap:model`.

.. rubric:: The Identifier Selector

While the **Model Explorer** is a very convenient tool to organize all
the information in your model, the **Identifier Selector** allows you to
select and simultaneously view the attributes of groups of identifiers
that share certain functional aspects in your model. By mutual
comparison of the important attributes, such overviews may help you to
further structure and edit the contents of your model, or to discover
oversights in a formulation. The **Identifier Selector** is discussed in
full detail in :ref:`chap:view`

.. rubric:: The Page Manager

The **Page Manager** allows you to organize all end-user windows
associated with an AIMMS application (also referred to as end-user
pages) in a tree-like fashion. The organization of pages in the page
tree directly defines the navigational structure of the end-user
interface. Relative to a particular page in the page tree, the positions
of the other pages define common relationships such as *parent* page,
*child* page, *next* page or *previous* page, which can used in
navigational controls such as buttons and menus. The **Page Manager** is
discussed in full detail in :ref:`sec:pagetool.pageman`.

.. rubric:: The Template Manager

Within the **Template Manager**, you can make sure that all end-user
pages have the same size and possess the same look and feel. You can
accomplish this by creating page templates which define the page
properties and objects common to a group of end-user pages, and by
subsequently placing all end-user pages into the tree of page templates.
The **Template Manager** is discussed in full detail in
:ref:`sec:pagetool.template`.

.. rubric:: The Menu Builder

With the **Menu Builder** you can create customized menu bars, pop-up
menus and toolbars that can be linked to either template pages or
end-user pages in your application. In the menu builder window you can
define menus and toolbars in a tree-like structure similar to the other
page-related tools, to indicate the hierarchical ordering of menus,
submenus and menu items. The **Menu Builder** is discussed in full
detail in :ref:`sec:pagetool.menu`.

.. _sec:start.dockable:

Dockable Windows
----------------

.. rubric:: Support for dockable windows

Dockable windows are an ideal means to keep frequently used tool windows
in an development environment permanently visible, and are a common part
of modern **I**\ ntegrated **D**\ evelopment **E**\ nvironments (IDE)
such as *Visual Studio .NET*.

.. rubric:: Docking states

Dockable windows can be used in a *docked*, *auto-hidden*, or *floating*
state. Whether a dockable window is in a docked, auto-hidden or floating
state can be changed at runtime through drag-and-drop.

.. rubric:: Docked windows

When docked, the tool windows are attached to the left, right, top or
bottom edge of the client area of the main AIMMS window. By default, all
modeling tools discussed in :ref:`sec:start.tools` are docked at the
left edge of the AIMMS window, as illustrated in
:numref:`fig:start.docking`.

.. figure:: docking-new.png
   :alt: Example of a **Model Explorer** docked at the left edge
   :name: fig:start.docking

   Example of a **Model Explorer** docked at the left edge

.. rubric:: Tool windows persistence

AIMMS automatically stores the location and size of each tool window.
This information is used to restore the location and size of each tool
window whenever an AIMMS session is started.

.. rubric:: Dragging windows

By dragging the windows caption of a docked window and moving the cursor
around the edges of the AIMMS window, you can move the docked window to
another position. While hovering over a drop target, a blue rectangle
(as illustrated in Figure  :numref:`fig:start.dnd`) snaps into place at
the appropriate location, whenever a dockable window is ready to be
docked at the location corresponding to the drop target. The area of a
docked window can also be split into two by dragging another dockable
window into the upper, lower, left or right part of docked window. In
all these cases, a blue rectangle shows how a dockable window will be
docked when you release the mouse at that time.

.. figure:: drop-targets.png
   :alt: Drag-and-drop windows
   :name: fig:start.dnd

   Drag-and-drop windows

.. rubric:: Auto-hidden windows

In auto-hidden state, a dockable window is normally collapsed as a
button to the upper, lower, left or right edge of the main AIMMS window.
When you push the button associated with a collapsed window, it is
expanded. When an expanded tool window looses focus, it is collapsed
again. By clicking the pushpin button |auto-hide| in the caption of a
docked/collapsed window, you can change the window's state from docked
to auto-hide and back.

.. rubric:: Floating tool windows

By dragging a tool window away from an edge of the main AIMMS window, it
becomes floating. When AIMMS is the active application, floating tool
windows are always visible on top of all other (non-floating) AIMMS
windows. Floating windows can also be shown outside the main AIMMS
window frame.

.. rubric:: Tabbed MDI mode

By default, all pages and attribute forms are shown in *tabbed MDI*
mode. In :numref:`fig:start.docking` the main page of the application is
displayed in tabbed MDI mode, with the attribute windows of two
identifiers in the model accessible through tabs. Tabbed MDI windows
occupy the remaining space of the client area of the main AIMMS window
that is not occupied by docked windows. This implies that you do not
have control over the size of tabbed MDI windows. Therefore, if you use
tabbed MDI mode in your AIMMS application, it makes sense to make all
the pages in your model resizable (see :ref:`chap:resize`). However, the
display mode of a page can be changed to docked (by checking the **Allow
User Dockable** option on the **Page Properties** dialog box). This
would for example allow you to turn a page into a floating window and
display it on you second monitor.

.. rubric:: Tab groups

When you drag the tab associated with a document window in the document
window area, you can move the document window into a new or existing
*tab group*, at the left, right, top or bottom of the current tab group.
Tab groups effectively split the document window area into multiple
parts, each of which can hold one ore more tabbed MDI windows. As with
dragging dockable windows, a drag rectangle shows where the window will
be positioned if you drop it at that moment. Tab groups are very
convenient, for instance, if you want to view two attribute windows
simultaneously.

.. _sec:start.files:

Additional Files Related to an AIMMS Project
--------------------------------------------

.. rubric:: Project-related files
   :name: project-files

In addition to the AIMMS project folders and files associated discussed
in :ref:`sec:start.new`, using an AIMMS project either during
development or in a deployment scenario may actually result in the
creation of a number of files not mentioned before:

-  the name change file (with a ``.nch`` extension),

-  one or more case files (with a ``.data`` extension),

-  a user database file (with a ``.usr`` extension),

-  data backup files (with a ``.bak`` extension),

-  log, error and listing files from both AIMMS and its solvers (with
   ``.log``, ``.err``, ``.lis`` or ``.sta`` extensions).

.. rubric:: Name change file

AIMMS has the capability to keep track of the name changes you performed
on identifiers in your model, and automatically replace an old
identifier name by its new one whenever AIMMS encounters a renamed
identifier. AIMMS keeps track of the list of such name changes in the
name change file (with a ``.nch`` extension). Each name change is listed
in this file on a separate line containing the old name, the new name
and the (GMT) time stamp at which the change was actually performed. The
automatic name change capabilities of AIMMS are explained in full detail
in :ref:`sec:decl.commit`

.. rubric:: ... and version control

If you are using a version control system to manage your AIMMS sources,
it makes sense to also include the name change files under version
control. When you change an identifier name, AIMMS will not directly
refactor all pages to reflect the name change directly, but use the name
change file to refactor a page when it is opened. The same is true when
opening cases that contain data for the identifier the name of which has
been changed. When your changes in a project are merged with another
developer's changes, the merged name change file will actually contain
all name changes made by both developers.

.. rubric:: Case files

Whenever you save a case in your AIMMS project (see also
:ref:`chap:cases`), this will result in the creation of a ``.data`` file
on disk. By default these case files will be stored in the ``data``
subfolder of project's main folder.

.. rubric:: The user database

With every end-user project created with AIMMS, you can associate an
end-user database containing the names and passwords of all end-users
entitled to run the project or view its results. Such end-user
information is stored in an encrypted format in a user database file
(with a ``.usr`` extension). You can use a single user database file
with more than one project.

.. rubric:: Log, error and listing files

During the execution of your model, all log, error and listing
information from both AIMMS and its solvers (whether visible in the
AIMMS **Message** window or not) is copied to log, error and listing
files, which, by default, are stored in the :any:`Log` subdirectory of the
project directory. If you are not interested in this information, you
can reduce the amount of information that is copied to these log files
by modifying the relevant execution options.

.. rubric:: Data backups

Through the **AutoSave & Backups-Data** menu, you can specify that you
want AIMMS to automatically create backups of the data used during a
particular session of your project. The menu will pop up the **Data
Backup** dialog box illustrated in :numref:`fig:start.data-backup`.

.. figure:: data-backup-new.png
   :alt: The **Data Backup** dialog box
   :name: fig:start.data-backup

   The **Data Backup** dialog box

Similarly as with the project backup files, you can indicate whether
AIMMS should automatically create backup backup files of the session
data at regular intervals, as well as how many data backup files should
be retained. Data backup files also have the ``.bak`` extension and
contain a reference to the date/time of the backup.

.. rubric:: Manually creating backup files

Besides the automated backup scheme built into AIMMS, you can also
create backup files of your session data manually. You can create manual
backup files through the **File-Data Backups** menu. When you create a
data backup file manually, AIMMS will request a name of a ``.bak`` file
in which the backup is to be stored.

.. rubric:: Restoring backup files

Through the **File-Data Backups** menu, you can restore the data in your
application back to the state stored in the data backup files.

.. _sec:start.files.user:

Project User Files
~~~~~~~~~~~~~~~~~~

.. rubric:: Project user files

Along with the project-related files created by AIMMS, you may need to
distribute some other files with your project when deploying it to your
end-users. Such files include, for instance, bitmap files displayed on
buttons or in the background of your end-user pages, or files that
contain project-related configuration data. Instead of having to include
such files as separate files in the project directory, AIMMS also allows
you to save them within the project file itself. Both within the AIMMS
language as well as in the end-user interface, you can reference such
*project user files* as if they were ordinary files on disk.

.. rubric:: Why use project user files?

User project files are convenient in a number of situations. The most
common reasons to store files as project user files are listed below.

-  You want to reduce the number files that you have to ship to your end
   users. This situation commonly occurs, for instance, when the
   end-user interface of your project references a large number of
   bitmap files.

-  You want to hide particular configuration data files from your
   end-users, which might otherwise only confuse them.

-  User project cannot be modified by your end-users.

.. rubric:: Importing project user files

You can import files into the project file through the **Tools-Project
User Files** menu, which will pop up the **Project User Files** dialog
box illustrated in :numref:`fig:start.user-files`.

.. figure:: projectuserfiles-new.png
   :alt: The **Project User Files** dialog box
   :name: fig:start.user-files

   The **Project User Files** dialog box

In this dialog box, you can create new folders to organize the files you
want to import into the project file. The dialog box of
:numref:`fig:start.user-files` already contains a folder **bitmaps**,
which is automatically added to each new AIMMS project and filled by
AIMMS with the bitmaps used on AIMMS' data pages (see
:ref:`sec:decl.data`). When you are inside a folder (or just within the
main project file), you can import a file into it through the **Import
File** button, which will open an ordinary file selection dialog box to
select the disk file to be imported.

.. rubric:: User files in library projects

When your project, next to the main project file, also includes a number
of library project files (see :ref:`sec:proj-organization.manager`),
AIMMS allows you to store user files in the library project files as
well. Thus, if a page defined in a library refers to a particular bitmap
file, you can also store that bitmap as a user file directly into the
corresponding library project file. In the dialog box of
:numref:`fig:start.user-files`, the *CoreModel* node at the root of the
tree refers to a library that is included in the project that serves as
the running example throughout this book. Underneath this node you can
add user files that will be stored in the library project file for the
*CoreModel* library.

.. rubric:: Referencing project user files

You can reference project user files both from within the AIMMS language
and the properties of various objects with the graphical end-user
interface. The basic rule is that AIMMS considers the project file as a
virtual disk indicated by "``<prj>``". You can use this virtual drive
in, for instance, ``READ``, ``WRITE`` and ``PUT`` statements within your
model. Thus, the statement

.. code-block:: aimms

	READ from file "<prj>:config\\english.dat";

reads the model data from the project user file ``"english.dat"``
contained in a (developer-created) **config** folder within the project
file.

.. rubric:: Referencing user files in library projects

You can access project files in library projects by using the virtual
disk notation "``<lib:``\ *library-name*\ ``>``", where *library-name*
is the name of the library project. Thus, to read the same file as in
the previous paragraph from the *CoreModel* library shown in
:numref:`fig:start.user-files`, the following statement can be used.

.. code-block:: aimms

	READ from file "<lib:CoreModel>:config\\english.dat";

.. rubric:: Use in end-user interface

Similarly, you can reference project user files on page objects in the
end-user interface of your project.
:numref:`fig:start.user-project-button` illustrates the use of a bitmap
file stored in the project file on a bitmap button.

.. figure:: projectuserbitmapbutton-new.png
   :alt: Bitmap button referencing a project user file
   :name: fig:start.user-project-button

   Bitmap button referencing a project user file

For all object properties expecting a file name (such as the **File
Name** property of the bitmap button illustrated in
:numref:`fig:start.user-project-button`), you can easily select a
project user file by pressing the wizard button |wizard|, and
selecting the **Select Project File** menu item. This will pop up a
project user file selection dialog box similar to the dialog box shown
in :numref:`fig:start.user-files`.

.. |wizard| image:: wizard.png

.. |auto-hide| image:: auto-hide.png


